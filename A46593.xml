<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A46593">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Majesties resolution for the calling of a free Parliament upon the humble motion and advice of a great council of his peers.</title>
    <author>James II, King of England, 1633-1701.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A46593 of text R31540 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing J380). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A46593</idno>
    <idno type="STC">Wing J380</idno>
    <idno type="STC">ESTC R31540</idno>
    <idno type="EEBO-CITATION">12083774</idno>
    <idno type="OCLC">ocm 12083774</idno>
    <idno type="VID">53681</idno>
    <idno type="PROQUESTGOID">2240943735</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A46593)</note>
    <note>Transcribed from: (Early English Books Online ; image set 53681)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1012:11)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Majesties resolution for the calling of a free Parliament upon the humble motion and advice of a great council of his peers.</title>
      <author>James II, King of England, 1633-1701.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1688]</date>
     </publicationStmt>
     <notesStmt>
      <note>Imprint suggested by Wing.</note>
      <note>Reproduction of original in the Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament.</term>
     <term>Great Britain -- History -- James II, 1685-1688.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>His Majesties resolution for the calling of a free Parliament, upon the humble motion, and advice of a great council of his peers.</ep:title>
    <ep:author>James II, King of England, </ep:author>
    <ep:publicationYear>1688</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>395</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-11</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-01</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-02</date><label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change><date>2008-02</date><label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A46593-t">
  <body xml:id="A46593-e0">
   <div type="resolution" xml:id="A46593-e10">
    <pb facs="tcp:53681:1" xml:id="A46593-001-a"/>
    <head xml:id="A46593-e20">
     <w lemma="his" pos="po" xml:id="A46593-001-a-0010">HIS</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A46593-001-a-0020">Majesties</w>
     <w lemma="resolution" pos="n1" xml:id="A46593-001-a-0030">Resolution</w>
     <w lemma="for" pos="acp" xml:id="A46593-001-a-0040">For</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-0050">the</w>
     <w lemma="call" pos="vvg" xml:id="A46593-001-a-0060">Calling</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-0070">of</w>
     <w lemma="a" pos="d" xml:id="A46593-001-a-0080">a</w>
     <w lemma="free" pos="j" xml:id="A46593-001-a-0090">FREE</w>
     <w lemma="parliament" pos="n1" xml:id="A46593-001-a-0100">PARLIAMENT</w>
     <pc xml:id="A46593-001-a-0110">,</pc>
     <w lemma="upon" pos="acp" xml:id="A46593-001-a-0120">Upon</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-0130">the</w>
     <w lemma="humble" pos="j" xml:id="A46593-001-a-0140">Humble</w>
     <w lemma="motion" pos="n1" xml:id="A46593-001-a-0150">Motion</w>
     <pc xml:id="A46593-001-a-0160">,</pc>
     <w lemma="and" pos="cc" xml:id="A46593-001-a-0170">and</w>
     <w lemma="advice" pos="n1" xml:id="A46593-001-a-0180">Advice</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-0190">of</w>
     <w lemma="a" pos="d" xml:id="A46593-001-a-0200">a</w>
     <w lemma="great" pos="j" xml:id="A46593-001-a-0210">Great</w>
     <w lemma="council" pos="n1" xml:id="A46593-001-a-0220">Council</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-0230">of</w>
     <w lemma="his" pos="po" xml:id="A46593-001-a-0240">his</w>
     <w lemma="peer" pos="n2" xml:id="A46593-001-a-0250">Peers</w>
     <pc unit="sentence" xml:id="A46593-001-a-0260">.</pc>
    </head>
    <p xml:id="A46593-e30">
     <w lemma="his" pos="po" xml:id="A46593-001-a-0270">His</w>
     <w lemma="majesty" pos="n1" xml:id="A46593-001-a-0280">Majesty</w>
     <w lemma="be" pos="vvd" xml:id="A46593-001-a-0290">was</w>
     <w lemma="gracious" pos="av-j" xml:id="A46593-001-a-0300">Graciously</w>
     <w lemma="please" pos="vvn" xml:id="A46593-001-a-0310">pleased</w>
     <pc xml:id="A46593-001-a-0320">,</pc>
     <w lemma="upon" pos="acp" xml:id="A46593-001-a-0330">upon</w>
     <hi xml:id="A46593-e40">
      <w lemma="Tuesday" pos="nn1" xml:id="A46593-001-a-0340">Tuesday</w>
      <pc xml:id="A46593-001-a-0350">,</pc>
      <w lemma="nou." pos="ab" reg="Nou." xml:id="A46593-001-a-0360">Nov.</w>
     </hi>
     <w lemma="27." pos="crd" xml:id="A46593-001-a-0370">27.</w>
     <w lemma="1688." pos="crd" xml:id="A46593-001-a-0380">1688.</w>
     <w lemma="to" pos="prt" xml:id="A46593-001-a-0390">to</w>
     <w lemma="advise" pos="vvi" xml:id="A46593-001-a-0400">advise</w>
     <w lemma="with" pos="acp" xml:id="A46593-001-a-0410">with</w>
     <w lemma="a" pos="d" xml:id="A46593-001-a-0420">a</w>
     <w lemma="council" pos="n1" xml:id="A46593-001-a-0430">Council</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-0440">of</w>
     <w lemma="his" pos="po" xml:id="A46593-001-a-0450">his</w>
     <w lemma="peer" pos="n2" xml:id="A46593-001-a-0460">Peers</w>
     <pc xml:id="A46593-001-a-0470">,</pc>
     <w lemma="about" pos="acp" xml:id="A46593-001-a-0480">about</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-0490">the</w>
     <w lemma="present" pos="j" xml:id="A46593-001-a-0500">present</w>
     <w lemma="unhappy" pos="j" xml:id="A46593-001-a-0510">Unhappy</w>
     <w lemma="state" pos="n1" xml:id="A46593-001-a-0520">State</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-0530">of</w>
     <w lemma="his" pos="po" xml:id="A46593-001-a-0540">his</w>
     <w lemma="affair" pos="n2" xml:id="A46593-001-a-0550">Affairs</w>
     <pc xml:id="A46593-001-a-0560">,</pc>
     <w lemma="where" pos="crq" xml:id="A46593-001-a-0570">where</w>
     <w lemma="they" pos="pns" xml:id="A46593-001-a-0580">they</w>
     <w lemma="attend" pos="vvd" xml:id="A46593-001-a-0590">attended</w>
     <w lemma="his" pos="po" xml:id="A46593-001-a-0600">his</w>
     <w lemma="royal" pos="j" xml:id="A46593-001-a-0610">Royal</w>
     <w lemma="pleasure" pos="n1" xml:id="A46593-001-a-0620">Pleasure</w>
     <w lemma="according" pos="j" xml:id="A46593-001-a-0630">according</w>
     <w lemma="to" pos="acp" xml:id="A46593-001-a-0640">to</w>
     <w lemma="a" pos="d" xml:id="A46593-001-a-0650">a</w>
     <w lemma="summons" pos="n1" xml:id="A46593-001-a-0660">Summons</w>
     <w lemma="unto" pos="acp" xml:id="A46593-001-a-0670">unto</w>
     <w lemma="they" pos="pno" xml:id="A46593-001-a-0680">them</w>
     <w lemma="direct" pos="vvn" xml:id="A46593-001-a-0690">directed</w>
     <pc unit="sentence" xml:id="A46593-001-a-0700">.</pc>
     <w lemma="there" pos="acp" xml:id="A46593-001-a-0710">There</w>
     <w lemma="be" pos="vvd" xml:id="A46593-001-a-0720">were</w>
     <w lemma="present" pos="j" xml:id="A46593-001-a-0730">present</w>
     <pc xml:id="A46593-001-a-0740">,</pc>
     <w lemma="twenty" pos="crd" xml:id="A46593-001-a-0750">twenty</w>
     <w lemma="five" pos="crd" xml:id="A46593-001-a-0760">five</w>
     <w lemma="protestant" pos="jnn" xml:id="A46593-001-a-0770">Protestant</w>
     <w lemma="temporal" pos="j" xml:id="A46593-001-a-0780">Temporal</w>
     <w lemma="lord" pos="n2" xml:id="A46593-001-a-0790">Lords</w>
     <pc xml:id="A46593-001-a-0800">,</pc>
     <w lemma="and" pos="cc" xml:id="A46593-001-a-0810">and</w>
     <w lemma="nine" pos="crd" xml:id="A46593-001-a-0820">nine</w>
     <w lemma="lord" pos="n2" xml:id="A46593-001-a-0830">Lords</w>
     <w lemma="spiritual" pos="j" xml:id="A46593-001-a-0840">Spiritual</w>
     <pc xml:id="A46593-001-a-0850">,</pc>
     <w lemma="be" pos="vvg" xml:id="A46593-001-a-0860">being</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-0870">the</w>
     <w lemma="whole" pos="j" xml:id="A46593-001-a-0880">whole</w>
     <w lemma="number" pos="n1" xml:id="A46593-001-a-0890">number</w>
     <w lemma="that" pos="cs" xml:id="A46593-001-a-0900">that</w>
     <w lemma="be" pos="vvd" xml:id="A46593-001-a-0910">were</w>
     <w lemma="within" pos="acp" xml:id="A46593-001-a-0920">within</w>
     <w lemma="distance" pos="n1" xml:id="A46593-001-a-0930">distance</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-0940">of</w>
     <w lemma="appear" pos="vvg" xml:id="A46593-001-a-0950">appearing</w>
     <w lemma="according" pos="j" xml:id="A46593-001-a-0960">according</w>
     <w lemma="to" pos="acp" xml:id="A46593-001-a-0970">to</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-0980">the</w>
     <w lemma="time" pos="n1" xml:id="A46593-001-a-0990">time</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-1000">of</w>
     <w lemma="summons" pos="n1" xml:id="A46593-001-a-1010">Summons</w>
     <pc unit="sentence" xml:id="A46593-001-a-1020">.</pc>
     <w lemma="his" pos="po" xml:id="A46593-001-a-1030">His</w>
     <w lemma="majesty" pos="n1" xml:id="A46593-001-a-1040">Majesty</w>
     <w lemma="be" pos="vvd" xml:id="A46593-001-a-1050">was</w>
     <w lemma="please" pos="vvn" xml:id="A46593-001-a-1060">pleased</w>
     <w lemma="to" pos="prt" xml:id="A46593-001-a-1070">to</w>
     <w lemma="express" pos="vvi" xml:id="A46593-001-a-1080">express</w>
     <w lemma="himself" pos="pr" xml:id="A46593-001-a-1090">himself</w>
     <w lemma="with" pos="acp" xml:id="A46593-001-a-1100">with</w>
     <w lemma="great" pos="j" xml:id="A46593-001-a-1110">great</w>
     <w lemma="clearness" pos="n1" xml:id="A46593-001-a-1120">Clearness</w>
     <w lemma="and" pos="cc" xml:id="A46593-001-a-1130">and</w>
     <w lemma="respect" pos="n1" xml:id="A46593-001-a-1140">Respect</w>
     <w lemma="upon" pos="acp" xml:id="A46593-001-a-1150">upon</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-1160">the</w>
     <w lemma="subject" pos="j" xml:id="A46593-001-a-1170">Subject</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-1180">of</w>
     <w lemma="that" pos="d" xml:id="A46593-001-a-1190">that</w>
     <w lemma="meeting" pos="n1" xml:id="A46593-001-a-1200">Meeting</w>
     <pc xml:id="A46593-001-a-1210">,</pc>
     <w lemma="require" pos="vvg" xml:id="A46593-001-a-1220">requiring</w>
     <w lemma="their" pos="po" xml:id="A46593-001-a-1230">their</w>
     <w lemma="lordship" pos="n2" xml:id="A46593-001-a-1240">Lordships</w>
     <w lemma="to" pos="prt" xml:id="A46593-001-a-1250">to</w>
     <w lemma="deliver" pos="vvi" xml:id="A46593-001-a-1260">deliver</w>
     <w lemma="their" pos="po" xml:id="A46593-001-a-1270">their</w>
     <w lemma="judgement" pos="n2" reg="Judgements" xml:id="A46593-001-a-1280">Judgments</w>
     <w lemma="with" pos="acp" xml:id="A46593-001-a-1290">with</w>
     <w lemma="all" pos="d" xml:id="A46593-001-a-1300">all</w>
     <w lemma="freedom" pos="n1" reg="Freedom" xml:id="A46593-001-a-1310">Freedome</w>
     <w lemma="upon" pos="acp" xml:id="A46593-001-a-1320">upon</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-1330">the</w>
     <w lemma="matter" pos="n1" xml:id="A46593-001-a-1340">Matter</w>
     <w lemma="in" pos="acp" xml:id="A46593-001-a-1350">in</w>
     <w lemma="hand" pos="n1" xml:id="A46593-001-a-1360">hand</w>
     <pc xml:id="A46593-001-a-1370">,</pc>
     <w lemma="wherein" pos="crq" xml:id="A46593-001-a-1380">wherein</w>
     <w lemma="they" pos="pns" xml:id="A46593-001-a-1390">they</w>
     <w lemma="acquit" pos="vvn" xml:id="A46593-001-a-1400">acquitted</w>
     <w lemma="themselves" pos="pr" xml:id="A46593-001-a-1410">themselves</w>
     <w lemma="according" pos="av-j" xml:id="A46593-001-a-1420">accordingly</w>
     <w lemma="upon" pos="acp" xml:id="A46593-001-a-1430">upon</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-1440">the</w>
     <w lemma="several" pos="j" xml:id="A46593-001-a-1450">several</w>
     <w lemma="point" pos="n2" xml:id="A46593-001-a-1460">Points</w>
     <w lemma="that" pos="cs" xml:id="A46593-001-a-1470">that</w>
     <w lemma="they" pos="pns" xml:id="A46593-001-a-1480">they</w>
     <w lemma="have" pos="vvd" xml:id="A46593-001-a-1490">had</w>
     <w lemma="in" pos="acp" xml:id="A46593-001-a-1500">in</w>
     <w lemma="debate" pos="zz" xml:id="A46593-001-a-1510">Debate</w>
     <pc xml:id="A46593-001-a-1520">;</pc>
     <w lemma="and" pos="cc" xml:id="A46593-001-a-1530">and</w>
     <w lemma="upon" pos="acp" xml:id="A46593-001-a-1540">upon</w>
     <w lemma="a" pos="d" xml:id="A46593-001-a-1550">a</w>
     <w lemma="full" pos="j" xml:id="A46593-001-a-1560">full</w>
     <w lemma="consideration" pos="n1" xml:id="A46593-001-a-1570">Consideration</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-1580">of</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-1590">the</w>
     <w lemma="whole" pos="j" xml:id="A46593-001-a-1600">whole</w>
     <w lemma="affair" pos="n1" xml:id="A46593-001-a-1610">Affair</w>
     <pc xml:id="A46593-001-a-1620">,</pc>
     <w lemma="their" pos="po" xml:id="A46593-001-a-1630">their</w>
     <w lemma="lordship" pos="n2" xml:id="A46593-001-a-1640">Lordships</w>
     <w lemma="come" pos="vvd" xml:id="A46593-001-a-1650">came</w>
     <w lemma="to" pos="acp" xml:id="A46593-001-a-1660">to</w>
     <w lemma="this" pos="d" xml:id="A46593-001-a-1670">this</w>
     <w lemma="resolution" pos="n1" xml:id="A46593-001-a-1680">Resolution</w>
     <pc xml:id="A46593-001-a-1690">,</pc>
     <pc join="right" xml:id="A46593-001-a-1700">(</pc>
     <w lemma="without" pos="acp" xml:id="A46593-001-a-1710">without</w>
     <w lemma="so" pos="av" xml:id="A46593-001-a-1720">so</w>
     <w lemma="much" pos="av-d" xml:id="A46593-001-a-1730">much</w>
     <w lemma="as" pos="acp" xml:id="A46593-001-a-1740">as</w>
     <w lemma="one" pos="crd" xml:id="A46593-001-a-1750">one</w>
     <w lemma="negative" pos="j" xml:id="A46593-001-a-1760">Negative</w>
     <pc xml:id="A46593-001-a-1770">)</pc>
     <w lemma="that" pos="cs" xml:id="A46593-001-a-1780">that</w>
     <w lemma="they" pos="pns" xml:id="A46593-001-a-1790">they</w>
     <w lemma="see" pos="vvd" xml:id="A46593-001-a-1800">saw</w>
     <w lemma="no" pos="dx" xml:id="A46593-001-a-1810">no</w>
     <w lemma="way" pos="n1" xml:id="A46593-001-a-1820">way</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-1830">of</w>
     <w lemma="redress" pos="n1" xml:id="A46593-001-a-1840">Redress</w>
     <pc xml:id="A46593-001-a-1850">,</pc>
     <w lemma="consider" pos="vvg" xml:id="A46593-001-a-1860">considering</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-1870">the</w>
     <w lemma="grievance" pos="n2" xml:id="A46593-001-a-1880">Grievances</w>
     <w lemma="and" pos="cc" xml:id="A46593-001-a-1890">and</w>
     <w lemma="distemper" pos="n2" xml:id="A46593-001-a-1900">Distempers</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-1910">of</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-1920">the</w>
     <w lemma="people" pos="n1" xml:id="A46593-001-a-1930">People</w>
     <pc xml:id="A46593-001-a-1940">,</pc>
     <w lemma="and" pos="cc" xml:id="A46593-001-a-1950">and</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-1960">the</w>
     <w lemma="present" pos="j" xml:id="A46593-001-a-1970">present</w>
     <w lemma="circumstance" pos="n2" xml:id="A46593-001-a-1980">Circumstances</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-1990">of</w>
     <w lemma="his" pos="po" xml:id="A46593-001-a-2000">his</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A46593-001-a-2010">Majesties</w>
     <w lemma="condition" pos="n1" xml:id="A46593-001-a-2020">Condition</w>
     <pc xml:id="A46593-001-a-2030">,</pc>
     <w lemma="but" pos="acp" xml:id="A46593-001-a-2040">but</w>
     <w lemma="by" pos="acp" xml:id="A46593-001-a-2050">by</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-2060">the</w>
     <w lemma="speedy" pos="j" xml:id="A46593-001-a-2070">speedy</w>
     <w lemma="call" pos="n1-vg" xml:id="A46593-001-a-2080">calling</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-2090">of</w>
     <w lemma="a" pos="d" xml:id="A46593-001-a-2100">a</w>
     <w lemma="free" pos="j" xml:id="A46593-001-a-2110">Free</w>
     <w lemma="parliament" pos="n1" xml:id="A46593-001-a-2120">Parliament</w>
     <pc xml:id="A46593-001-a-2130">,</pc>
     <w lemma="which" pos="crq" xml:id="A46593-001-a-2140">which</w>
     <w lemma="their" pos="po" xml:id="A46593-001-a-2150">their</w>
     <w lemma="lordship" pos="n2" xml:id="A46593-001-a-2160">Lordships</w>
     <w lemma="with" pos="acp" xml:id="A46593-001-a-2170">with</w>
     <w lemma="one" pos="crd" xml:id="A46593-001-a-2180">one</w>
     <w lemma="voice" pos="n1" xml:id="A46593-001-a-2190">voice</w>
     <w lemma="humble" pos="av-j" xml:id="A46593-001-a-2200">humbly</w>
     <w lemma="beseech" pos="vvd" xml:id="A46593-001-a-2210">besought</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-2220">the</w>
     <w lemma="king" pos="n1" xml:id="A46593-001-a-2230">King</w>
     <w lemma="to" pos="prt" xml:id="A46593-001-a-2240">to</w>
     <w lemma="do" pos="vvi" xml:id="A46593-001-a-2250">do</w>
     <pc xml:id="A46593-001-a-2260">:</pc>
     <w lemma="whereupon" pos="crq" xml:id="A46593-001-a-2270">Whereupon</w>
     <w lemma="his" pos="po" xml:id="A46593-001-a-2280">his</w>
     <w lemma="majesty" pos="n1" xml:id="A46593-001-a-2290">Majesty</w>
     <w lemma="be" pos="vvd" xml:id="A46593-001-a-2300">was</w>
     <w lemma="please" pos="vvn" xml:id="A46593-001-a-2310">pleased</w>
     <w lemma="to" pos="prt" xml:id="A46593-001-a-2320">to</w>
     <w lemma="declare" pos="vvi" xml:id="A46593-001-a-2330">Declare</w>
     <w lemma="to" pos="acp" xml:id="A46593-001-a-2340">to</w>
     <w lemma="their" pos="po" xml:id="A46593-001-a-2350">their</w>
     <w lemma="lordship" pos="n2" xml:id="A46593-001-a-2360">Lordships</w>
     <pc xml:id="A46593-001-a-2370">,</pc>
     <w lemma="to" pos="prt" xml:id="A46593-001-a-2380">to</w>
     <w lemma="this" pos="d" xml:id="A46593-001-a-2390">this</w>
     <w lemma="effect" pos="n1" xml:id="A46593-001-a-2400">Effect</w>
     <pc xml:id="A46593-001-a-2410">;</pc>
     <w lemma="that" pos="cs" xml:id="A46593-001-a-2420">that</w>
     <w lemma="their" pos="po" xml:id="A46593-001-a-2430">their</w>
     <w lemma="advice" pos="n1" xml:id="A46593-001-a-2440">Advice</w>
     <w lemma="be" pos="vvd" xml:id="A46593-001-a-2450">was</w>
     <w lemma="according" pos="j" xml:id="A46593-001-a-2460">according</w>
     <w lemma="to" pos="acp" xml:id="A46593-001-a-2470">to</w>
     <w lemma="his" pos="po" xml:id="A46593-001-a-2480">his</w>
     <w lemma="own" pos="d" xml:id="A46593-001-a-2490">own</w>
     <w lemma="inclination" pos="n2" xml:id="A46593-001-a-2500">Inclinations</w>
     <pc xml:id="A46593-001-a-2510">,</pc>
     <w lemma="but" pos="acp" xml:id="A46593-001-a-2520">but</w>
     <w lemma="it" pos="pn" xml:id="A46593-001-a-2530">it</w>
     <w lemma="be" pos="vvg" xml:id="A46593-001-a-2540">being</w>
     <w lemma="a" pos="d" xml:id="A46593-001-a-2550">a</w>
     <w lemma="matter" pos="n1" xml:id="A46593-001-a-2560">Matter</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-2570">of</w>
     <w lemma="so" pos="av" xml:id="A46593-001-a-2580">so</w>
     <w lemma="great" pos="j" xml:id="A46593-001-a-2590">great</w>
     <w lemma="a" pos="d" xml:id="A46593-001-a-2600">an</w>
     <w lemma="importance" pos="n1" xml:id="A46593-001-a-2610">Importance</w>
     <pc xml:id="A46593-001-a-2620">,</pc>
     <w lemma="he" pos="pns" xml:id="A46593-001-a-2630">he</w>
     <w lemma="will" pos="vmd" xml:id="A46593-001-a-2640">would</w>
     <w lemma="take" pos="vvi" xml:id="A46593-001-a-2650">take</w>
     <w lemma="time" pos="n1" xml:id="A46593-001-a-2660">time</w>
     <w lemma="till" pos="acp" reg="till" xml:id="A46593-001-a-2670">til</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-2680">the</w>
     <w lemma="next" pos="ord" xml:id="A46593-001-a-2690">next</w>
     <w lemma="day" pos="n1" xml:id="A46593-001-a-2700">day</w>
     <w lemma="to" pos="prt" xml:id="A46593-001-a-2710">to</w>
     <w lemma="consider" pos="vvi" xml:id="A46593-001-a-2720">consider</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-2730">of</w>
     <w lemma="it" pos="pn" xml:id="A46593-001-a-2740">it</w>
     <pc unit="sentence" xml:id="A46593-001-a-2750">.</pc>
     <w lemma="and" pos="cc" xml:id="A46593-001-a-2760">And</w>
     <w lemma="upon" pos="acp" xml:id="A46593-001-a-2770">upon</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-2780">the</w>
     <w lemma="day" pos="n1" xml:id="A46593-001-a-2790">day</w>
     <w lemma="follow" pos="vvg" xml:id="A46593-001-a-2800">following</w>
     <pc xml:id="A46593-001-a-2810">,</pc>
     <w lemma="his" pos="po" xml:id="A46593-001-a-2820">his</w>
     <w lemma="majesty" pos="n1" xml:id="A46593-001-a-2830">Majesty</w>
     <w lemma="be" pos="vvd" xml:id="A46593-001-a-2840">was</w>
     <w lemma="please" pos="vvn" xml:id="A46593-001-a-2850">pleased</w>
     <w lemma="to" pos="acp" xml:id="A46593-001-a-2860">to</w>
     <w lemma="order" pos="n1" xml:id="A46593-001-a-2870">Order</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-2880">the</w>
     <w lemma="issue" pos="vvg" xml:id="A46593-001-a-2890">Issuing</w>
     <w lemma="out" pos="av" xml:id="A46593-001-a-2900">out</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-2910">of</w>
     <w lemma="writ" pos="n2" xml:id="A46593-001-a-2920">Writs</w>
     <w lemma="for" pos="acp" xml:id="A46593-001-a-2930">for</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-2940">the</w>
     <w lemma="call" pos="n1-vg" xml:id="A46593-001-a-2950">calling</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-2960">of</w>
     <w lemma="a" pos="d" xml:id="A46593-001-a-2970">a</w>
     <w lemma="free" pos="j" xml:id="A46593-001-a-2980">Free</w>
     <w lemma="parliament" pos="n1" xml:id="A46593-001-a-2990">Parliament</w>
     <pc unit="sentence" xml:id="A46593-001-a-3000">.</pc>
    </p>
    <p xml:id="A46593-e50">
     <w lemma="there" pos="acp" xml:id="A46593-001-a-3010">There</w>
     <w lemma="be" pos="vvd" xml:id="A46593-001-a-3020">were</w>
     <w lemma="present" pos="j" xml:id="A46593-001-a-3030">present</w>
     <pc xml:id="A46593-001-a-3040">,</pc>
     <w lemma="his" pos="po" xml:id="A46593-001-a-3050">His</w>
     <w lemma="grace" pos="n1" xml:id="A46593-001-a-3060">Grace</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-3070">the</w>
     <w lemma="archbishop" pos="n1" reg="Archbishop" xml:id="A46593-001-a-3080">Arch-Bishop</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-3090">of</w>
     <hi xml:id="A46593-e60">
      <w lemma="Canterbury" pos="nn1" xml:id="A46593-001-a-3100">Canterbury</w>
      <pc xml:id="A46593-001-a-3110">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A46593-001-a-3120">and</w>
     <w lemma="his" pos="po" xml:id="A46593-001-a-3130">His</w>
     <w lemma="grace" pos="n1" xml:id="A46593-001-a-3140">Grace</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-3150">of</w>
     <hi xml:id="A46593-e70">
      <w lemma="Tork" pos="nn1" xml:id="A46593-001-a-3160">Tork</w>
     </hi>
     <w lemma="in" pos="acp" xml:id="A46593-001-a-3170">in</w>
     <w lemma="nomination" pos="n1" xml:id="A46593-001-a-3180">Nomination</w>
     <pc xml:id="A46593-001-a-3190">,</pc>
     <w lemma="with" pos="acp" xml:id="A46593-001-a-3200">with</w>
     <w lemma="seven" pos="crd" xml:id="A46593-001-a-3210">seven</w>
     <w lemma="prelate" pos="n2" xml:id="A46593-001-a-3220">Prelates</w>
     <w lemma="more" pos="avc-d" xml:id="A46593-001-a-3230">more</w>
     <pc unit="sentence" xml:id="A46593-001-a-3240">.</pc>
    </p>
    <p xml:id="A46593-e80">
     <w lemma="the" pos="d" xml:id="A46593-001-a-3250">The</w>
     <w lemma="right" pos="j" xml:id="A46593-001-a-3260">Right</w>
     <w lemma="honourable" pos="j" xml:id="A46593-001-a-3270">Honourable</w>
     <w lemma="the" pos="d" xml:id="A46593-001-a-3280">the</w>
     <w lemma="lord" pos="n1" xml:id="A46593-001-a-3290">Lord</w>
     <hi xml:id="A46593-e90">
      <w lemma="chancellor" pos="n1" reg="Chancellor" xml:id="A46593-001-a-3300">Chancellour</w>
      <pc xml:id="A46593-001-a-3310">,</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A46593-001-a-3320">the</w>
     <w lemma="marquis" pos="n1" reg="Marquis" xml:id="A46593-001-a-3330">Marquiss</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-3340">of</w>
     <hi xml:id="A46593-e100">
      <w lemma="Hallifax" pos="nn1" xml:id="A46593-001-a-3350">Hallifax</w>
      <pc xml:id="A46593-001-a-3360">,</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A46593-001-a-3370">the</w>
     <w lemma="earl" pos="n2" xml:id="A46593-001-a-3380">Earls</w>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-3390">of</w>
     <hi xml:id="A46593-e110">
      <w lemma="Oxford" pos="nn1" xml:id="A46593-001-a-3400">Oxford</w>
      <pc xml:id="A46593-001-a-3410">,</pc>
      <w lemma="Pembroke" pos="nn1" reg="Pembroke" xml:id="A46593-001-a-3420">Pembrook</w>
      <pc xml:id="A46593-001-a-3430">,</pc>
      <w lemma="clarendon" pos="nn1" xml:id="A46593-001-a-3440">Clarendon</w>
      <pc xml:id="A46593-001-a-3450">,</pc>
      <w lemma="Anglesey" pos="nn1" xml:id="A46593-001-a-3460">Anglesey</w>
      <pc xml:id="A46593-001-a-3470">,</pc>
      <w lemma="Carlisle" pos="nn1" xml:id="A46593-001-a-3480">Carlisle</w>
      <pc xml:id="A46593-001-a-3490">,</pc>
      <w lemma="craven" pos="nn1" xml:id="A46593-001-a-3500">Craven</w>
      <pc xml:id="A46593-001-a-3510">,</pc>
      <w lemma="aylesbury" pos="nn1" xml:id="A46593-001-a-3520">Aylesbury</w>
      <pc xml:id="A46593-001-a-3530">,</pc>
      <w lemma="burlington" pos="nn1" xml:id="A46593-001-a-3540">Burlington</w>
      <pc xml:id="A46593-001-a-3550">,</pc>
      <w lemma="Yarmouth" pos="nn1" xml:id="A46593-001-a-3560">Yarmouth</w>
      <pc xml:id="A46593-001-a-3570">,</pc>
      <w lemma="berkley" pos="nn1" xml:id="A46593-001-a-3580">Berkley</w>
      <pc xml:id="A46593-001-a-3590">,</pc>
      <w lemma="Nottingham" pos="nn1" xml:id="A46593-001-a-3600">Nottingham</w>
      <pc xml:id="A46593-001-a-3610">,</pc>
      <w lemma="Rochester" pos="nn1" xml:id="A46593-001-a-3620">Rochester</w>
      <pc xml:id="A46593-001-a-3630">,</pc>
     </hi>
     <w lemma="viscount" pos="n1" reg="Viscount" xml:id="A46593-001-a-3640">Vicount</w>
     <hi xml:id="A46593-e120">
      <w lemma="Falconberg" pos="nn1" xml:id="A46593-001-a-3650">Falconberg</w>
      <pc xml:id="A46593-001-a-3660">,</pc>
     </hi>
     <w lemma="viscount" pos="n1" reg="Viscount" xml:id="A46593-001-a-3670">Vicount</w>
     <hi xml:id="A46593-e130">
      <w lemma="Newport" pos="nn1" xml:id="A46593-001-a-3680">Newport</w>
      <pc xml:id="A46593-001-a-3690">,</pc>
     </hi>
     <w lemma="viscount" pos="n1" reg="Viscount" xml:id="A46593-001-a-3700">Vicount</w>
     <hi xml:id="A46593-e140">
      <w lemma="weymouth" pos="nn1" xml:id="A46593-001-a-3710">Weymouth</w>
      <pc unit="sentence" xml:id="A46593-001-a-3720">.</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A46593-001-a-3730">The</w>
     <w lemma="lord" pos="n2" xml:id="A46593-001-a-3740">Lords</w>
     <pc xml:id="A46593-001-a-3750">,</pc>
     <hi xml:id="A46593-e150">
      <w lemma="Chandois" pos="nn1" xml:id="A46593-001-a-3760">Chandois</w>
      <pc xml:id="A46593-001-a-3770">,</pc>
      <w lemma="Paget" pos="nn1" xml:id="A46593-001-a-3780">Paget</w>
      <pc xml:id="A46593-001-a-3790">,</pc>
      <w lemma="Vaughan" pos="nn1" xml:id="A46593-001-a-3800">Vaughan</w>
      <pc xml:id="A46593-001-a-3810">,</pc>
      <w lemma="Montague" pos="nn1" xml:id="A46593-001-a-3820">Montague</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-3830">of</w>
     <hi xml:id="A46593-e160">
      <w lemma="boughton" pos="nn1" xml:id="A46593-001-a-3840">Boughton</w>
      <pc xml:id="A46593-001-a-3850">,</pc>
      <w lemma="maynard" pos="nn1" xml:id="A46593-001-a-3860">Maynard</w>
      <pc xml:id="A46593-001-a-3870">,</pc>
      <w lemma="Howard" pos="nn1" xml:id="A46593-001-a-3880">Howard</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="A46593-001-a-3890">of</w>
     <hi xml:id="A46593-e170">
      <w lemma="escrick" pos="nn1" xml:id="A46593-001-a-3900">Escrick</w>
      <pc xml:id="A46593-001-a-3910">,</pc>
      <w lemma="Ossulston" pos="nn1" xml:id="A46593-001-a-3920">Ossulston</w>
      <pc xml:id="A46593-001-a-3930">,</pc>
      <w lemma="godolphin" pos="nn1" xml:id="A46593-001-a-3940">Godolphin</w>
      <pc unit="sentence" xml:id="A46593-001-a-3950">.</pc>
     </hi>
    </p>
   </div>
  </body>
 </text>
</TEI>
