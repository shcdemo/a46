<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A46135">
 <teiHeader xml:id="A46135-A46135-header">
  <fileDesc>
   <titleStmt>
    <title>Whereas divers persons who had passed certificates from His Majesties late commissioners appointed for putting in execution the Acts of Setlement ... by the Lord Lieutenant and Council, J. Roberts.</title>
    <author>Ireland. Lord Lieutenant (1669-1670 : Radnor)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A46135 of text R36874 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing I771). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A46135</idno>
    <idno type="STC">Wing I771</idno>
    <idno type="STC">ESTC R36874</idno>
    <idno type="EEBO-CITATION">16146402</idno>
    <idno type="OCLC">ocm 16146402</idno>
    <idno type="VID">104859</idno>
    <idno type="PROQUESTGOID">2240855788</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A46135)</note>
    <note>Transcribed from: (Early English Books Online ; image set 104859)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1595:21)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Whereas divers persons who had passed certificates from His Majesties late commissioners appointed for putting in execution the Acts of Setlement ... by the Lord Lieutenant and Council, J. Roberts.</title>
      <author>Ireland. Lord Lieutenant (1669-1670 : Radnor)</author>
      <author>Radnor, John Robartes, Earl of, 1606-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by Benjamin Tooke ... and are to be sold by Samuel Dancer ...,</publisher>
      <pubPlace>Dublin :</pubPlace>
      <date>1670.</date>
     </publicationStmt>
     <notesStmt>
      <note>Title from first 3 lines of text.</note>
      <note>Statement of responsibility transposed from head of title.</note>
      <note>"Given at the Council chamber in Dublin, the twentieth eight day of March 1670."</note>
      <note>Reproduction of original in the Society of Antiquaries Library, London.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Letters patent -- Ireland.</term>
     <term>Ireland -- History -- 1649-1775.</term>
     <term>Ireland -- Politics and government -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>Whereas divers persons who had passed certificates from His Majesties late commissioners appointed for putting in execution the Acts of Setlement ...</ep:title>
    <ep:author>Ireland. Lord Lieutenant </ep:author>
    <ep:publicationYear>1670</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>292</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-11</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-01</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-03</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2008-03</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A46135-A46135-text">
  <body xml:id="A46135-e0">
   <div type="text" xml:id="A46135-e10">
    <head xml:id="A46135-e20">
     <figure xml:id="A46135-e30">
      <p xml:id="A46135-e40">
       <w lemma="cr" pos="sy" xml:id="A46135-0050">CR</w>
      </p>
      <p xml:id="A46135-e50">
       <w lemma="diev" pos="ffr" xml:id="A46135-0080">DIEV</w>
       <w lemma="et" pos="ffr" xml:id="A46135-0090">ET</w>
       <w lemma="mon" pos="ffr" xml:id="A46135-0100">MON</w>
       <w lemma="droit" pos="ffr" xml:id="A46135-0110">DROIT</w>
      </p>
      <p xml:id="A46135-e60">
       <w lemma="honi" pos="ffr" xml:id="A46135-0140">HONI</w>
       <w lemma="soit" pos="ffr" xml:id="A46135-0150">SOIT</w>
       <w lemma="qvi" pos="ffr" xml:id="A46135-0160">QVI</w>
       <w lemma="mal" pos="ffr" xml:id="A46135-0170">MAL</w>
       <w lemma="y" pos="ffr" xml:id="A46135-0180">Y</w>
       <w lemma="pense" pos="ffr" xml:id="A46135-0190">PENSE</w>
      </p>
      <figDesc xml:id="A46135-e70">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <pb facs="tcp:104859:1" xml:id="A46135-001-a"/>
    <head xml:id="A46135-e80">
     <w lemma="by" pos="acp" xml:id="A46135-001-a-0010">BY</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-0020">THE</w>
     <w lemma="lord" pos="n1" xml:id="A46135-001-a-0030">LORD</w>
     <w lemma="lieutenant" pos="n1" xml:id="A46135-001-a-0040">LIEUTENANT</w>
     <w lemma="and" pos="cc" xml:id="A46135-001-a-0050">AND</w>
     <w lemma="council" pos="n1" xml:id="A46135-001-a-0060">COUNCIL</w>
     <pc unit="sentence" xml:id="A46135-001-a-0070">.</pc>
    </head>
    <opener xml:id="A46135-e90">
     <signed xml:id="A46135-e100">
      <w lemma="j." pos="ab" xml:id="A46135-001-a-0080">J.</w>
      <w lemma="ROBERTS" pos="nn1" xml:id="A46135-001-a-0090">ROBERTS</w>
      <pc xml:id="A46135-001-a-0100">,</pc>
     </signed>
    </opener>
    <p xml:id="A46135-e110">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A46135-001-a-0110">WHEREAS</w>
     <w lemma="divers" pos="j" xml:id="A46135-001-a-0120">divers</w>
     <w lemma="person" pos="n2" xml:id="A46135-001-a-0130">persons</w>
     <w lemma="who" pos="crq" xml:id="A46135-001-a-0140">who</w>
     <w lemma="have" pos="vvd" xml:id="A46135-001-a-0150">had</w>
     <w lemma="pass" pos="vvn" xml:id="A46135-001-a-0160">passed</w>
     <w lemma="certificate" pos="n2" xml:id="A46135-001-a-0170">Certificates</w>
     <w lemma="from" pos="acp" xml:id="A46135-001-a-0180">from</w>
     <w lemma="his" pos="po" xml:id="A46135-001-a-0190">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A46135-001-a-0200">Majesties</w>
     <w lemma="late" pos="j" xml:id="A46135-001-a-0210">late</w>
     <w lemma="commissioner" pos="n2" xml:id="A46135-001-a-0220">Commissioners</w>
     <w lemma="appoint" pos="vvn" xml:id="A46135-001-a-0230">appointed</w>
     <w lemma="for" pos="acp" xml:id="A46135-001-a-0240">for</w>
     <w lemma="put" pos="vvg" xml:id="A46135-001-a-0250">putting</w>
     <w lemma="in" pos="acp" xml:id="A46135-001-a-0260">in</w>
     <w lemma="execution" pos="n1" xml:id="A46135-001-a-0270">Execution</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-0280">the</w>
     <w lemma="act" pos="n2" xml:id="A46135-001-a-0290">Acts</w>
     <w lemma="of" pos="acp" xml:id="A46135-001-a-0300">of</w>
     <w lemma="settlement" pos="n1" reg="Settlement" xml:id="A46135-001-a-0310">Setlement</w>
     <w lemma="and" pos="cc" xml:id="A46135-001-a-0320">and</w>
     <w lemma="explanation" pos="n1" xml:id="A46135-001-a-0330">Explanation</w>
     <w lemma="of" pos="acp" xml:id="A46135-001-a-0340">of</w>
     <w lemma="such" pos="d" xml:id="A46135-001-a-0350">such</w>
     <w lemma="land" pos="n2" xml:id="A46135-001-a-0360">Lands</w>
     <pc xml:id="A46135-001-a-0370">,</pc>
     <w lemma="tenement" pos="n2" xml:id="A46135-001-a-0380">Tenements</w>
     <pc xml:id="A46135-001-a-0390">,</pc>
     <w lemma="and" pos="cc" xml:id="A46135-001-a-0400">and</w>
     <w lemma="hereditament" pos="n2" xml:id="A46135-001-a-0410">Hereditaments</w>
     <w lemma="as" pos="acp" xml:id="A46135-001-a-0420">as</w>
     <w lemma="in" pos="acp" xml:id="A46135-001-a-0430">in</w>
     <w lemma="pursuance" pos="n1" xml:id="A46135-001-a-0440">pursuance</w>
     <w lemma="of" pos="acp" xml:id="A46135-001-a-0450">of</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-0460">the</w>
     <w lemma="say" pos="j-vn" xml:id="A46135-001-a-0470">said</w>
     <w lemma="act" pos="n2" xml:id="A46135-001-a-0480">Acts</w>
     <w lemma="have" pos="vvd" xml:id="A46135-001-a-0490">had</w>
     <w lemma="be" pos="vvn" xml:id="A46135-001-a-0500">been</w>
     <w lemma="adjudge" pos="vvn" xml:id="A46135-001-a-0510">adjudged</w>
     <w lemma="to" pos="prt" xml:id="A46135-001-a-0520">to</w>
     <w lemma="belong" pos="vvi" xml:id="A46135-001-a-0530">belong</w>
     <w lemma="unto" pos="acp" xml:id="A46135-001-a-0540">unto</w>
     <w lemma="they" pos="pno" xml:id="A46135-001-a-0550">them</w>
     <pc xml:id="A46135-001-a-0560">,</pc>
     <w lemma="have" pos="vvd" xml:id="A46135-001-a-0570">had</w>
     <w lemma="neglect" pos="vvn" xml:id="A46135-001-a-0580">neglected</w>
     <w lemma="to" pos="prt" xml:id="A46135-001-a-0590">to</w>
     <w lemma="pass" pos="vvi" xml:id="A46135-001-a-0600">pass</w>
     <w lemma="their" pos="po" xml:id="A46135-001-a-0610">their</w>
     <w lemma="letter" pos="n2" xml:id="A46135-001-a-0620">Letters</w>
     <w lemma="patent" pos="n2" xml:id="A46135-001-a-0630">Patents</w>
     <w lemma="thereupon" pos="av" xml:id="A46135-001-a-0640">thereupon</w>
     <pc xml:id="A46135-001-a-0650">,</pc>
     <w lemma="by" pos="acp" xml:id="A46135-001-a-0660">by</w>
     <w lemma="reason" pos="n1" xml:id="A46135-001-a-0670">reason</w>
     <w lemma="whereof" pos="crq" xml:id="A46135-001-a-0680">whereof</w>
     <w lemma="his" pos="po" xml:id="A46135-001-a-0690">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A46135-001-a-0700">Majesties</w>
     <w lemma="revenue" pos="n1" xml:id="A46135-001-a-0710">Revenue</w>
     <w lemma="arise" pos="vvg" xml:id="A46135-001-a-0720">arising</w>
     <w lemma="by" pos="acp" xml:id="A46135-001-a-0730">by</w>
     <w lemma="his" pos="po" xml:id="A46135-001-a-0740">his</w>
     <w lemma="new" pos="j" xml:id="A46135-001-a-0750">new</w>
     <w lemma="quitrent" pos="n2" reg="Quitrents" xml:id="A46135-001-a-0760">Quit-Rents</w>
     <w lemma="be" pos="vvd" xml:id="A46135-001-a-0770">was</w>
     <w lemma="very" pos="av" xml:id="A46135-001-a-0780">very</w>
     <w lemma="much" pos="d" xml:id="A46135-001-a-0790">much</w>
     <w lemma="unsettle" pos="j-vn" reg="unsettled" xml:id="A46135-001-a-0800">unsetled</w>
     <pc xml:id="A46135-001-a-0810">;</pc>
     <w lemma="we" pos="pns" xml:id="A46135-001-a-0820">We</w>
     <w lemma="therefore" pos="av" xml:id="A46135-001-a-0830">therefore</w>
     <w lemma="on" pos="acp" xml:id="A46135-001-a-0840">on</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-0850">the</w>
     <w lemma="26" pos="ord" rend="hi" xml:id="A46135-001-a-0860">26th</w>
     <w lemma="of" pos="acp" xml:id="A46135-001-a-0870">of</w>
     <w lemma="November" pos="nn1" rend="hi" xml:id="A46135-001-a-0880">November</w>
     <w lemma="1669." pos="crd" xml:id="A46135-001-a-0890">1669.</w>
     <w lemma="do" pos="vvd" xml:id="A46135-001-a-0900">did</w>
     <w lemma="order" pos="n1" xml:id="A46135-001-a-0910">order</w>
     <w lemma="and" pos="cc" xml:id="A46135-001-a-0920">and</w>
     <w lemma="declare" pos="vvi" xml:id="A46135-001-a-0930">declare</w>
     <w lemma="that" pos="cs" xml:id="A46135-001-a-0940">that</w>
     <w lemma="all" pos="d" xml:id="A46135-001-a-0950">all</w>
     <w lemma="person" pos="n2" xml:id="A46135-001-a-0960">persons</w>
     <w lemma="who" pos="crq" xml:id="A46135-001-a-0970">who</w>
     <w lemma="have" pos="vvd" xml:id="A46135-001-a-0980">had</w>
     <w lemma="pass" pos="vvn" xml:id="A46135-001-a-0990">passed</w>
     <w lemma="any" pos="d" xml:id="A46135-001-a-1000">any</w>
     <w lemma="certificate" pos="n2" xml:id="A46135-001-a-1010">Certificates</w>
     <w lemma="from" pos="acp" xml:id="A46135-001-a-1020">from</w>
     <w lemma="his" pos="po" xml:id="A46135-001-a-1030">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A46135-001-a-1040">Majesties</w>
     <w lemma="say" pos="j-vn" xml:id="A46135-001-a-1050">said</w>
     <w lemma="commissioner" pos="n2" xml:id="A46135-001-a-1060">Commissioners</w>
     <w lemma="of" pos="acp" xml:id="A46135-001-a-1070">of</w>
     <w lemma="any" pos="d" xml:id="A46135-001-a-1080">any</w>
     <w lemma="land" pos="n2" xml:id="A46135-001-a-1090">Lands</w>
     <pc xml:id="A46135-001-a-1100">,</pc>
     <w lemma="tenement" pos="n2" xml:id="A46135-001-a-1110">Tenements</w>
     <pc xml:id="A46135-001-a-1120">,</pc>
     <w lemma="or" pos="cc" xml:id="A46135-001-a-1130">or</w>
     <w lemma="hereditament" pos="n2" xml:id="A46135-001-a-1140">Hereditaments</w>
     <w lemma="to" pos="prt" xml:id="A46135-001-a-1150">to</w>
     <w lemma="be" pos="vvi" xml:id="A46135-001-a-1160">be</w>
     <w lemma="settle" pos="vvn" reg="settled" xml:id="A46135-001-a-1170">setled</w>
     <w lemma="upon" pos="acp" xml:id="A46135-001-a-1180">upon</w>
     <pc xml:id="A46135-001-a-1190">,</pc>
     <w lemma="or" pos="cc" xml:id="A46135-001-a-1200">or</w>
     <w lemma="restore" pos="vvn" xml:id="A46135-001-a-1210">restored</w>
     <w lemma="unto" pos="acp" xml:id="A46135-001-a-1220">unto</w>
     <w lemma="they" pos="pno" xml:id="A46135-001-a-1230">them</w>
     <pc xml:id="A46135-001-a-1240">,</pc>
     <w lemma="shall" pos="vmd" xml:id="A46135-001-a-1250">should</w>
     <w lemma="at" pos="acp" xml:id="A46135-001-a-1260">at</w>
     <w lemma="their" pos="po" xml:id="A46135-001-a-1270">their</w>
     <w lemma="peril" pos="n2" reg="perils" xml:id="A46135-001-a-1280">perills</w>
     <w lemma="pass" pos="vvi" xml:id="A46135-001-a-1290">pass</w>
     <w lemma="their" pos="po" xml:id="A46135-001-a-1300">their</w>
     <w lemma="letter" pos="n2" xml:id="A46135-001-a-1310">Letters</w>
     <w lemma="patent" pos="n2" xml:id="A46135-001-a-1320">patents</w>
     <w lemma="thereupon" pos="av" xml:id="A46135-001-a-1330">thereupon</w>
     <w lemma="within" pos="acp" xml:id="A46135-001-a-1340">within</w>
     <w lemma="four" pos="crd" xml:id="A46135-001-a-1350">four</w>
     <w lemma="month" pos="n2" reg="months" xml:id="A46135-001-a-1360">moneths</w>
     <w lemma="from" pos="acp" xml:id="A46135-001-a-1370">from</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-1380">the</w>
     <w lemma="date" pos="n1" xml:id="A46135-001-a-1390">date</w>
     <w lemma="thereof" pos="av" xml:id="A46135-001-a-1400">thereof</w>
     <pc unit="sentence" xml:id="A46135-001-a-1410">.</pc>
     <w lemma="now" pos="av" xml:id="A46135-001-a-1420">Now</w>
     <w lemma="at" pos="acp" xml:id="A46135-001-a-1430">at</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-1440">the</w>
     <w lemma="humble" pos="j" xml:id="A46135-001-a-1450">humble</w>
     <w lemma="petition" pos="n1" xml:id="A46135-001-a-1460">petition</w>
     <w lemma="of" pos="acp" xml:id="A46135-001-a-1470">of</w>
     <w lemma="several" pos="j" reg="several" xml:id="A46135-001-a-1480">severall</w>
     <w lemma="adventurer" pos="n2" xml:id="A46135-001-a-1490">Adventurers</w>
     <pc xml:id="A46135-001-a-1500">,</pc>
     <w lemma="soldier" pos="n2" reg="Soldiers" xml:id="A46135-001-a-1510">Souldiers</w>
     <pc xml:id="A46135-001-a-1520">,</pc>
     <w lemma="and" pos="cc" xml:id="A46135-001-a-1530">and</w>
     <w lemma="other" pos="pi2-d" xml:id="A46135-001-a-1540">others</w>
     <pc xml:id="A46135-001-a-1550">;</pc>
     <w lemma="we" pos="pns" xml:id="A46135-001-a-1560">We</w>
     <w lemma="think" pos="vvb" xml:id="A46135-001-a-1570">think</w>
     <w lemma="fit" pos="j" xml:id="A46135-001-a-1580">fit</w>
     <w lemma="hereby" pos="av" xml:id="A46135-001-a-1590">hereby</w>
     <w lemma="to" pos="acp" xml:id="A46135-001-a-1600">to</w>
     <w lemma="order" pos="n1" xml:id="A46135-001-a-1610">order</w>
     <w lemma="and" pos="cc" xml:id="A46135-001-a-1620">and</w>
     <w lemma="declare" pos="vvi" xml:id="A46135-001-a-1630">declare</w>
     <pc xml:id="A46135-001-a-1640">,</pc>
     <w lemma="that" pos="cs" xml:id="A46135-001-a-1650">that</w>
     <w lemma="all" pos="d" xml:id="A46135-001-a-1660">all</w>
     <w lemma="person" pos="n2" xml:id="A46135-001-a-1670">persons</w>
     <w lemma="as" pos="acp" xml:id="A46135-001-a-1680">as</w>
     <w lemma="aforesaid" pos="j" xml:id="A46135-001-a-1690">aforesaid</w>
     <w lemma="have" pos="vvb" xml:id="A46135-001-a-1700">have</w>
     <w lemma="three" pos="crd" xml:id="A46135-001-a-1710">three</w>
     <w lemma="month" pos="n2" reg="months" xml:id="A46135-001-a-1720">moneths</w>
     <w lemma="further" pos="jc" xml:id="A46135-001-a-1730">further</w>
     <w lemma="time" pos="n1" xml:id="A46135-001-a-1740">time</w>
     <w lemma="from" pos="acp" xml:id="A46135-001-a-1750">from</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-1760">the</w>
     <w lemma="date" pos="n1" xml:id="A46135-001-a-1770">date</w>
     <w lemma="hereof" pos="av" xml:id="A46135-001-a-1780">hereof</w>
     <pc xml:id="A46135-001-a-1790">,</pc>
     <w lemma="for" pos="acp" xml:id="A46135-001-a-1800">for</w>
     <w lemma="pass" pos="vvg" xml:id="A46135-001-a-1810">passing</w>
     <w lemma="letter" pos="n2" xml:id="A46135-001-a-1820">Letters</w>
     <w lemma="patent" pos="n2" xml:id="A46135-001-a-1830">patents</w>
     <w lemma="upon" pos="acp" xml:id="A46135-001-a-1840">upon</w>
     <w lemma="certificate" pos="n2" xml:id="A46135-001-a-1850">Certificates</w>
     <w lemma="of" pos="acp" xml:id="A46135-001-a-1860">of</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-1870">the</w>
     <w lemma="late" pos="j" xml:id="A46135-001-a-1880">late</w>
     <w lemma="commissioner" pos="n2" xml:id="A46135-001-a-1890">Commissioners</w>
     <w lemma="as" pos="acp" xml:id="A46135-001-a-1900">as</w>
     <w lemma="aforesaid" pos="j" xml:id="A46135-001-a-1910">aforesaid</w>
     <pc xml:id="A46135-001-a-1920">,</pc>
     <w lemma="within" pos="acp" xml:id="A46135-001-a-1930">within</w>
     <w lemma="which" pos="crq" xml:id="A46135-001-a-1940">which</w>
     <w lemma="time" pos="n1" xml:id="A46135-001-a-1950">time</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-1960">the</w>
     <w lemma="say" pos="j-vn" xml:id="A46135-001-a-1970">said</w>
     <w lemma="letter" pos="n2" xml:id="A46135-001-a-1980">Letters</w>
     <w lemma="patent" pos="n2" xml:id="A46135-001-a-1990">patents</w>
     <w lemma="be" pos="vvb" xml:id="A46135-001-a-2000">are</w>
     <w lemma="to" pos="prt" xml:id="A46135-001-a-2010">to</w>
     <w lemma="be" pos="vvi" xml:id="A46135-001-a-2020">be</w>
     <w lemma="past" pos="j" xml:id="A46135-001-a-2030">past</w>
     <w lemma="at" pos="acp" xml:id="A46135-001-a-2040">at</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-2050">the</w>
     <w lemma="peril" pos="n2" reg="perils" xml:id="A46135-001-a-2060">perills</w>
     <w lemma="of" pos="acp" xml:id="A46135-001-a-2070">of</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-2080">the</w>
     <w lemma="person" pos="n2" xml:id="A46135-001-a-2090">persons</w>
     <w lemma="concern" pos="vvn" xml:id="A46135-001-a-2100">concerned</w>
     <pc unit="sentence" xml:id="A46135-001-a-2110">.</pc>
    </p>
    <closer xml:id="A46135-e140">
     <dateline xml:id="A46135-e150">
      <w lemma="give" pos="vvn" xml:id="A46135-001-a-2120">Given</w>
      <w lemma="at" pos="acp" xml:id="A46135-001-a-2130">at</w>
      <w lemma="the" pos="d" xml:id="A46135-001-a-2140">the</w>
      <w lemma="council" pos="n1" reg="Council" xml:id="A46135-001-a-2150">Councill</w>
      <w lemma="chamber" pos="n1" xml:id="A46135-001-a-2160">Chamber</w>
      <w lemma="in" pos="acp" xml:id="A46135-001-a-2170">in</w>
      <w lemma="Dublin" pos="nn1" rend="hi" xml:id="A46135-001-a-2180">Dublin</w>
      <pc xml:id="A46135-001-a-2190">,</pc>
      <date xml:id="A46135-e170">
       <w lemma="the" pos="d" xml:id="A46135-001-a-2200">the</w>
       <w lemma="twenty" pos="ord" xml:id="A46135-001-a-2210">twentieth</w>
       <w lemma="eight" pos="crd" xml:id="A46135-001-a-2220">eight</w>
       <w lemma="day" pos="n1" xml:id="A46135-001-a-2230">day</w>
       <w lemma="of" pos="acp" xml:id="A46135-001-a-2240">of</w>
       <w lemma="march" pos="n1" rend="hi" xml:id="A46135-001-a-2250">March</w>
       <w lemma="1670" pos="crd" xml:id="A46135-001-a-2260">1670</w>
      </date>
      <pc unit="sentence" xml:id="A46135-001-a-2270">.</pc>
     </dateline>
     <signed xml:id="A46135-e190">
      <list xml:id="A46135-e200">
       <item xml:id="A46135-e210">
        <w lemma="ja" pos="ab" xml:id="A46135-001-a-2280">Ja.</w>
        <w lemma="armachanus" pos="fla" xml:id="A46135-001-a-2290">Armachanus</w>
        <pc unit="sentence" xml:id="A46135-001-a-2300">.</pc>
       </item>
       <item xml:id="A46135-e220">
        <w lemma="mich." pos="ab" xml:id="A46135-001-a-2310">Mich.</w>
        <w lemma="Dublin" pos="nn1" xml:id="A46135-001-a-2320">Dublin</w>
        <pc unit="sentence" xml:id="A46135-001-a-2330">.</pc>
        <w lemma="canc." pos="ab" rend="hi" xml:id="A46135-001-a-2340">Canc.</w>
        <pc unit="sentence" xml:id="A46135-001-a-2350"/>
       </item>
       <item xml:id="A46135-e240">
        <w lemma="r." pos="ab" xml:id="A46135-001-a-2360">R.</w>
        <w lemma="Booth" pos="nn1" xml:id="A46135-001-a-2370">Booth</w>
        <pc unit="sentence" xml:id="A46135-001-a-2380">.</pc>
       </item>
       <item xml:id="A46135-e250">
        <w lemma="j." pos="ab" xml:id="A46135-001-a-2390">J.</w>
        <w lemma="temple" pos="n1" xml:id="A46135-001-a-2400">Temple</w>
        <pc unit="sentence" xml:id="A46135-001-a-2410">.</pc>
       </item>
       <item xml:id="A46135-e260">
        <w lemma="paul." pos="ab" reg="Paul" xml:id="A46135-001-a-2420">Paul.</w>
        <w lemma="Davys" pos="nn1" xml:id="A46135-001-a-2430">Davys</w>
        <pc unit="sentence" xml:id="A46135-001-a-2440">.</pc>
       </item>
       <item xml:id="A46135-e270">
        <w lemma="edw" pos="ab" xml:id="A46135-001-a-2450">Edw.</w>
        <w lemma="Massie" pos="nn1" xml:id="A46135-001-a-2460">Massie</w>
        <pc unit="sentence" xml:id="A46135-001-a-2470">.</pc>
       </item>
      </list>
     </signed>
     <lb xml:id="A46135-e280"/>
     <w lemma="God" pos="nn1" xml:id="A46135-001-a-2480">God</w>
     <w lemma="save" pos="acp" xml:id="A46135-001-a-2490">Save</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-2500">the</w>
     <w lemma="king." pos="ab" xml:id="A46135-001-a-2510">King.</w>
     <pc unit="sentence" xml:id="A46135-001-a-2520"/>
    </closer>
   </div>
  </body>
  <back xml:id="A46135-e290">
   <div type="colophon" xml:id="A46135-e300">
    <p xml:id="A46135-e310">
     <w lemma="Dublin" pos="nn1" rend="hi" xml:id="A46135-001-a-2530">Dublin</w>
     <pc xml:id="A46135-001-a-2540">,</pc>
     <w lemma="print" pos="vvn" xml:id="A46135-001-a-2550">Printed</w>
     <w lemma="by" pos="acp" xml:id="A46135-001-a-2560">by</w>
     <hi xml:id="A46135-e330">
      <w lemma="Benjamin" pos="nn1" xml:id="A46135-001-a-2570">Benjamin</w>
      <w lemma="take" pos="vvd" reg="Taken" xml:id="A46135-001-a-2580">Tooke</w>
     </hi>
     <pc rend="follows-hi" xml:id="A46135-001-a-2590">,</pc>
     <w lemma="printer" pos="n1" xml:id="A46135-001-a-2600">Printer</w>
     <w lemma="to" pos="acp" xml:id="A46135-001-a-2610">to</w>
     <w lemma="the" pos="d" xml:id="A46135-001-a-2620">the</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A46135-001-a-2630">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A46135-001-a-2640">most</w>
     <w lemma="excellent" pos="j" xml:id="A46135-001-a-2650">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A46135-001-a-2660">Majesty</w>
     <pc xml:id="A46135-001-a-2670">,</pc>
     <w lemma="and" pos="cc" xml:id="A46135-001-a-2680">and</w>
     <w lemma="be" pos="vvb" xml:id="A46135-001-a-2690">are</w>
     <w lemma="to" pos="prt" xml:id="A46135-001-a-2700">to</w>
     <w lemma="be" pos="vvi" xml:id="A46135-001-a-2710">be</w>
     <w lemma="sell" pos="vvn" xml:id="A46135-001-a-2720">sold</w>
     <w lemma="by" pos="acp" xml:id="A46135-001-a-2730">by</w>
     <hi xml:id="A46135-e340">
      <w lemma="Samuel" pos="nn1" xml:id="A46135-001-a-2740">Samuel</w>
      <w lemma="dancer" pos="n1" xml:id="A46135-001-a-2750">Dancer</w>
     </hi>
     <pc rend="follows-hi" xml:id="A46135-001-a-2760">,</pc>
     <w lemma="in" pos="acp" xml:id="A46135-001-a-2770">in</w>
     <w lemma="castlestreet" pos="n1" rend="hi" xml:id="A46135-001-a-2780">Castlestreet</w>
     <pc xml:id="A46135-001-a-2790">,</pc>
     <w lemma="1670." pos="crd" xml:id="A46135-001-a-2800">1670.</w>
     <pc unit="sentence" xml:id="A46135-001-a-2810"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
