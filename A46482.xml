<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A46482">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The King's letter to the general of his army with the general's letter to the Prince of Orange.</title>
    <author>James II, King of England, 1633-1701.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A46482 of text R25556 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing J205). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A46482</idno>
    <idno type="STC">Wing J205</idno>
    <idno type="STC">ESTC R25556</idno>
    <idno type="EEBO-CITATION">09012387</idno>
    <idno type="OCLC">ocm 09012387</idno>
    <idno type="VID">42231</idno>
    <idno type="PROQUESTGOID">2248517125</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A46482)</note>
    <note>Transcribed from: (Early English Books Online ; image set 42231)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1287:18)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The King's letter to the general of his army with the general's letter to the Prince of Orange.</title>
      <author>James II, King of England, 1633-1701.</author>
      <author>William III, King of England, 1650-1702.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London? :</pubPlace>
      <date>1688?]</date>
     </publicationStmt>
     <notesStmt>
      <note>Letters dated: White-Hall, Decemb. 11, 1688, and, Uxbridge, Decemb. 11, 1688.</note>
      <note>Reproduction of original in the Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Revolution of 1688.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>The King's letter to the general of his army: with the general's letter to the Prince of Orange.</ep:title>
    <ep:author>James II, King of England, </ep:author>
    <ep:publicationYear>1688</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>487</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-11</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-01</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-02</date><label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change><date>2008-08</date><label>SPi Global</label>
        Rekeyed and resubmitted
      </change>
   <change><date>2008-11</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2008-11</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A46482-t">
  <body xml:id="A46482-e0">
   <div type="letter" xml:id="A46482-e10">
    <pb facs="tcp:42231:1" xml:id="A46482-001-a"/>
    <head xml:id="A46482-e20">
     <w lemma="the" pos="d" xml:id="A46482-001-a-0010">The</w>
     <hi xml:id="A46482-e30">
      <w lemma="king" pos="ng1" xml:id="A46482-001-a-0020">KING'S</w>
     </hi>
     <w lemma="letter" pos="n1" xml:id="A46482-001-a-0030">Letter</w>
     <w lemma="to" pos="acp" xml:id="A46482-001-a-0040">to</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-0050">the</w>
     <w lemma="general" pos="n1" xml:id="A46482-001-a-0060">General</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-0070">of</w>
     <w lemma="his" pos="po" xml:id="A46482-001-a-0080">his</w>
     <w lemma="army" pos="n1" xml:id="A46482-001-a-0090">Army</w>
     <pc unit="sentence" xml:id="A46482-001-a-0100">.</pc>
     <w lemma="with" pos="acp" xml:id="A46482-001-a-0110">With</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-0120">the</w>
     <w lemma="general" pos="ng1" reg="General's" xml:id="A46482-001-a-0130">Generals</w>
     <w lemma="letter" pos="n1" xml:id="A46482-001-a-0140">Letter</w>
     <w lemma="to" pos="acp" xml:id="A46482-001-a-0150">to</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-0160">the</w>
     <w lemma="prince" pos="n1" xml:id="A46482-001-a-0170">Prince</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-0180">of</w>
     <hi xml:id="A46482-e40">
      <w lemma="Orange" pos="nn1" xml:id="A46482-001-a-0190">ORANGE</w>
      <pc unit="sentence" xml:id="A46482-001-a-0200">.</pc>
     </hi>
    </head>
    <opener xml:id="A46482-e50">
     <dateline xml:id="A46482-e60">
      <w lemma="white-hall" pos="nn1" xml:id="A46482-001-a-0210">White-Hall</w>
      <pc xml:id="A46482-001-a-0220">,</pc>
      <date xml:id="A46482-e70">
       <hi xml:id="A46482-e80">
        <w lemma="decemb." pos="ab" xml:id="A46482-001-a-0230">Decemb.</w>
       </hi>
       <w lemma="11." pos="crd" xml:id="A46482-001-a-0240">11.</w>
       <w lemma="1688." pos="crd" xml:id="A46482-001-a-0250">1688.</w>
       <pc unit="sentence" xml:id="A46482-001-a-0260"/>
      </date>
     </dateline>
    </opener>
    <p xml:id="A46482-e90">
     <w lemma="thing" pos="n2" xml:id="A46482-001-a-0270">THings</w>
     <w lemma="be" pos="vvg" xml:id="A46482-001-a-0280">being</w>
     <w lemma="come" pos="vvn" xml:id="A46482-001-a-0290">come</w>
     <w lemma="to" pos="acp" xml:id="A46482-001-a-0300">to</w>
     <w lemma="that" pos="d" xml:id="A46482-001-a-0310">that</w>
     <w lemma="extremity" pos="n1" xml:id="A46482-001-a-0320">Extremity</w>
     <pc xml:id="A46482-001-a-0330">,</pc>
     <w lemma="that" pos="cs" xml:id="A46482-001-a-0340">That</w>
     <w lemma="i" pos="pns" xml:id="A46482-001-a-0350">I</w>
     <w lemma="have" pos="vvb" xml:id="A46482-001-a-0360">have</w>
     <w lemma="be" pos="vvn" xml:id="A46482-001-a-0370">been</w>
     <w lemma="force" pos="vvn" reg="forced" xml:id="A46482-001-a-0380">forc'd</w>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-0390">to</w>
     <w lemma="send" pos="vvi" xml:id="A46482-001-a-0400">send</w>
     <w lemma="away" pos="av" xml:id="A46482-001-a-0410">away</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-0420">the</w>
     <w lemma="queen" pos="n1" xml:id="A46482-001-a-0430">Queen</w>
     <pc xml:id="A46482-001-a-0440">,</pc>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-0450">and</w>
     <w lemma="my" pos="po" xml:id="A46482-001-a-0460">my</w>
     <w lemma="son" pos="n1" xml:id="A46482-001-a-0470">Son</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-0480">the</w>
     <w lemma="prince" pos="n1" xml:id="A46482-001-a-0490">Prince</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-0500">of</w>
     <hi xml:id="A46482-e100">
      <w lemma="Wales" pos="nn1" xml:id="A46482-001-a-0510">Wales</w>
      <pc xml:id="A46482-001-a-0520">,</pc>
     </hi>
     <w lemma="that" pos="cs" xml:id="A46482-001-a-0530">That</w>
     <w lemma="they" pos="pns" xml:id="A46482-001-a-0540">they</w>
     <w lemma="may" pos="vmd" xml:id="A46482-001-a-0550">might</w>
     <w lemma="not" pos="xx" xml:id="A46482-001-a-0560">not</w>
     <w lemma="fall" pos="vvi" xml:id="A46482-001-a-0570">fall</w>
     <w lemma="into" pos="acp" xml:id="A46482-001-a-0580">into</w>
     <w lemma="my" pos="po" xml:id="A46482-001-a-0590">my</w>
     <w lemma="enemy" pos="ng1" reg="Enemy's" xml:id="A46482-001-a-0600">Enemies</w>
     <w lemma="hand" pos="n2" xml:id="A46482-001-a-0610">Hands</w>
     <pc join="right" xml:id="A46482-001-a-0620">(</pc>
     <w lemma="which" pos="crq" xml:id="A46482-001-a-0630">which</w>
     <w lemma="they" pos="pns" xml:id="A46482-001-a-0640">they</w>
     <w lemma="must" pos="vmb" xml:id="A46482-001-a-0650">must</w>
     <w lemma="have" pos="vvi" xml:id="A46482-001-a-0660">have</w>
     <w lemma="do" pos="vvn" xml:id="A46482-001-a-0670">done</w>
     <pc xml:id="A46482-001-a-0680">,</pc>
     <w lemma="have" pos="vvd" xml:id="A46482-001-a-0690">had</w>
     <w lemma="they" pos="pns" xml:id="A46482-001-a-0700">they</w>
     <w lemma="stay" pos="vvd" reg="stayed" xml:id="A46482-001-a-0710">stay'd</w>
     <pc xml:id="A46482-001-a-0720">)</pc>
     <w lemma="i" pos="pns" xml:id="A46482-001-a-0730">I</w>
     <w lemma="be" pos="vvm" xml:id="A46482-001-a-0740">am</w>
     <w lemma="oblige" pos="vvn" reg="obliged" xml:id="A46482-001-a-0750">oblig'd</w>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-0760">to</w>
     <w lemma="do" pos="vvi" xml:id="A46482-001-a-0770">do</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-0780">the</w>
     <w lemma="same" pos="d" xml:id="A46482-001-a-0790">same</w>
     <w lemma="thing" pos="n1" xml:id="A46482-001-a-0800">thing</w>
     <pc xml:id="A46482-001-a-0810">,</pc>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-0820">and</w>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-0830">to</w>
     <w lemma="endeavour" pos="vvi" xml:id="A46482-001-a-0840">endeavour</w>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-0850">to</w>
     <w lemma="fecure" pos="vvi" xml:id="A46482-001-a-0860">fecure</w>
     <w lemma="myself" pos="pr" xml:id="A46482-001-a-0870">myself</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-0880">the</w>
     <w lemma="best" pos="js" xml:id="A46482-001-a-0890">best</w>
     <w lemma="i" pos="pns" xml:id="A46482-001-a-0900">I</w>
     <w lemma="can" pos="vmb" xml:id="A46482-001-a-0910">can</w>
     <pc xml:id="A46482-001-a-0920">,</pc>
     <w lemma="in" pos="acp" xml:id="A46482-001-a-0930">in</w>
     <w lemma="hope" pos="n2" xml:id="A46482-001-a-0940">hopes</w>
     <w lemma="that" pos="cs" xml:id="A46482-001-a-0950">that</w>
     <w lemma="it" pos="pn" xml:id="A46482-001-a-0960">it</w>
     <w lemma="will" pos="vmb" xml:id="A46482-001-a-0970">will</w>
     <w lemma="please" pos="vvi" xml:id="A46482-001-a-0980">please</w>
     <w lemma="God" pos="nn1" xml:id="A46482-001-a-0990">God</w>
     <pc xml:id="A46482-001-a-1000">,</pc>
     <w lemma="out" pos="av" xml:id="A46482-001-a-1010">out</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-1020">of</w>
     <w lemma="his" pos="po" xml:id="A46482-001-a-1030">his</w>
     <w lemma="infinite" pos="j" xml:id="A46482-001-a-1040">infinite</w>
     <w lemma="mercy" pos="n1" xml:id="A46482-001-a-1050">Mercy</w>
     <w lemma="to" pos="acp" xml:id="A46482-001-a-1060">to</w>
     <w lemma="this" pos="d" xml:id="A46482-001-a-1070">this</w>
     <w lemma="unhappy" pos="j" xml:id="A46482-001-a-1080">unhappy</w>
     <w lemma="passion" pos="n1" xml:id="A46482-001-a-1090">Passion</w>
     <pc xml:id="A46482-001-a-1100">,</pc>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-1110">to</w>
     <w lemma="touch" pos="vvi" xml:id="A46482-001-a-1120">touch</w>
     <w lemma="their" pos="po" xml:id="A46482-001-a-1130">their</w>
     <w lemma="heart" pos="n2" xml:id="A46482-001-a-1140">Hearts</w>
     <w lemma="again" pos="av" xml:id="A46482-001-a-1150">again</w>
     <w lemma="with" pos="acp" xml:id="A46482-001-a-1160">with</w>
     <w lemma="true" pos="j" xml:id="A46482-001-a-1170">true</w>
     <w lemma="loyalty" pos="n1" xml:id="A46482-001-a-1180">Loyalty</w>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-1190">and</w>
     <w lemma="honour" pos="n1" xml:id="A46482-001-a-1200">Honour</w>
     <pc unit="sentence" xml:id="A46482-001-a-1210">.</pc>
     <w lemma="if" pos="cs" xml:id="A46482-001-a-1220">If</w>
     <hi xml:id="A46482-e110">
      <w lemma="i" pos="pns" xml:id="A46482-001-a-1230">I</w>
     </hi>
     <w lemma="can" pos="vmd" xml:id="A46482-001-a-1240">could</w>
     <w lemma="have" pos="vvi" xml:id="A46482-001-a-1250">have</w>
     <w lemma="rely" pos="vvn" xml:id="A46482-001-a-1260">relied</w>
     <w lemma="on" pos="acp" xml:id="A46482-001-a-1270">on</w>
     <w lemma="all" pos="d" xml:id="A46482-001-a-1280">all</w>
     <w lemma="my" pos="po" xml:id="A46482-001-a-1290">my</w>
     <w lemma="troop" pos="n2" xml:id="A46482-001-a-1300">Troops</w>
     <hi xml:id="A46482-e120">
      <w lemma="i" pos="pns" xml:id="A46482-001-a-1310">I</w>
     </hi>
     <w lemma="may" pos="vmd" xml:id="A46482-001-a-1320">might</w>
     <w lemma="not" pos="xx" xml:id="A46482-001-a-1330">not</w>
     <w lemma="have" pos="vvi" xml:id="A46482-001-a-1340">have</w>
     <w lemma="be" pos="vvn" xml:id="A46482-001-a-1350">been</w>
     <w lemma="put" pos="vvn" xml:id="A46482-001-a-1360">put</w>
     <w lemma="to" pos="acp" xml:id="A46482-001-a-1370">to</w>
     <w lemma="this" pos="d" xml:id="A46482-001-a-1380">this</w>
     <w lemma="extremity" pos="n1" xml:id="A46482-001-a-1390">Extremity</w>
     <hi xml:id="A46482-e130">
      <w lemma="i" pos="pns" xml:id="A46482-001-a-1400">I</w>
     </hi>
     <w lemma="be" pos="vvm" xml:id="A46482-001-a-1410">am</w>
     <w lemma="in" pos="acp" xml:id="A46482-001-a-1420">in</w>
     <pc xml:id="A46482-001-a-1430">;</pc>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-1440">and</w>
     <w lemma="will" pos="vmd" xml:id="A46482-001-a-1450">would</w>
     <pc xml:id="A46482-001-a-1460">,</pc>
     <w lemma="at" pos="acp" xml:id="A46482-001-a-1470">at</w>
     <w lemma="least" pos="ds" xml:id="A46482-001-a-1480">least</w>
     <pc xml:id="A46482-001-a-1490">,</pc>
     <w lemma="have" pos="vvb" xml:id="A46482-001-a-1500">have</w>
     <w lemma="have" pos="vvn" xml:id="A46482-001-a-1510">had</w>
     <w lemma="one" pos="crd" xml:id="A46482-001-a-1520">one</w>
     <w lemma="blow" pos="n1" xml:id="A46482-001-a-1530">Blow</w>
     <w lemma="for" pos="acp" xml:id="A46482-001-a-1540">for</w>
     <w lemma="it" pos="pn" xml:id="A46482-001-a-1550">it</w>
     <pc xml:id="A46482-001-a-1560">:</pc>
     <w lemma="but" pos="acp" xml:id="A46482-001-a-1570">But</w>
     <w lemma="though" pos="cs" xml:id="A46482-001-a-1580">though</w>
     <hi xml:id="A46482-e140">
      <w lemma="i" pos="pns" xml:id="A46482-001-a-1590">I</w>
     </hi>
     <w lemma="know" pos="vvb" xml:id="A46482-001-a-1600">know</w>
     <w lemma="there" pos="av" xml:id="A46482-001-a-1610">there</w>
     <w lemma="be" pos="vvb" xml:id="A46482-001-a-1620">are</w>
     <w lemma="among" pos="acp" xml:id="A46482-001-a-1630">amongst</w>
     <w lemma="you" pos="pn" xml:id="A46482-001-a-1640">you</w>
     <w lemma="very" pos="av" xml:id="A46482-001-a-1650">very</w>
     <w lemma="many" pos="d" xml:id="A46482-001-a-1660">many</w>
     <w lemma="loyal" pos="j" xml:id="A46482-001-a-1670">loyal</w>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-1680">and</w>
     <w lemma="brave" pos="j" xml:id="A46482-001-a-1690">brave</w>
     <w lemma="man" pos="n2" xml:id="A46482-001-a-1700">Men</w>
     <pc xml:id="A46482-001-a-1710">,</pc>
     <w lemma="both" pos="d" xml:id="A46482-001-a-1720">both</w>
     <w lemma="officer" pos="n2" xml:id="A46482-001-a-1730">Officers</w>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-1740">and</w>
     <w lemma="soldier" pos="n2" reg="Soldiers" xml:id="A46482-001-a-1750">Souldiers</w>
     <pc xml:id="A46482-001-a-1760">;</pc>
     <w lemma="yet" pos="av" xml:id="A46482-001-a-1770">yet</w>
     <w lemma="you" pos="pn" xml:id="A46482-001-a-1780">you</w>
     <w lemma="know" pos="vvb" xml:id="A46482-001-a-1790">know</w>
     <pc xml:id="A46482-001-a-1800">,</pc>
     <w lemma="that" pos="cs" xml:id="A46482-001-a-1810">That</w>
     <w lemma="both" pos="d" xml:id="A46482-001-a-1820">both</w>
     <w lemma="yourself" pos="pr" reg="yourself" xml:id="A46482-001-a-1830">your self</w>
     <pc xml:id="A46482-001-a-1850">,</pc>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-1860">and</w>
     <w lemma="several" pos="j" xml:id="A46482-001-a-1870">several</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-1880">of</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-1890">the</w>
     <w lemma="general" pos="j" xml:id="A46482-001-a-1900">General</w>
     <w lemma="officer" pos="n2" xml:id="A46482-001-a-1910">Officers</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-1920">of</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-1930">the</w>
     <w lemma="army" pos="n1" xml:id="A46482-001-a-1940">Army</w>
     <pc xml:id="A46482-001-a-1950">,</pc>
     <w lemma="tell" pos="vvd" xml:id="A46482-001-a-1960">told</w>
     <w lemma="i" pos="pno" xml:id="A46482-001-a-1970">me</w>
     <pc xml:id="A46482-001-a-1980">,</pc>
     <w lemma="it" pos="pn" xml:id="A46482-001-a-1990">it</w>
     <w lemma="be" pos="vvd" xml:id="A46482-001-a-2000">was</w>
     <w lemma="no" pos="dx" xml:id="A46482-001-a-2010">no</w>
     <w lemma="way" pos="n2" xml:id="A46482-001-a-2020">ways</w>
     <w lemma="advisable" pos="vvi" xml:id="A46482-001-a-2030">advisable</w>
     <w lemma="for" pos="acp" xml:id="A46482-001-a-2040">for</w>
     <w lemma="i" pos="pno" xml:id="A46482-001-a-2050">me</w>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-2060">to</w>
     <w lemma="venture" pos="vvi" xml:id="A46482-001-a-2070">venture</w>
     <w lemma="myself" pos="pr" reg="myself" xml:id="A46482-001-a-2080">my self</w>
     <w lemma="at" pos="acp" xml:id="A46482-001-a-2100">at</w>
     <w lemma="their" pos="po" xml:id="A46482-001-a-2110">their</w>
     <w lemma="head" pos="n1" xml:id="A46482-001-a-2120">Head</w>
     <pc xml:id="A46482-001-a-2130">,</pc>
     <w lemma="or" pos="cc" xml:id="A46482-001-a-2140">or</w>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-2150">to</w>
     <w lemma="think" pos="vvi" xml:id="A46482-001-a-2160">think</w>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-2170">to</w>
     <w lemma="fight" pos="vvi" xml:id="A46482-001-a-2180">fight</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-2190">the</w>
     <w lemma="prince" pos="n1" xml:id="A46482-001-a-2200">Prince</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-2210">of</w>
     <hi xml:id="A46482-e150">
      <w lemma="orange" pos="n1" xml:id="A46482-001-a-2220">Orange</w>
     </hi>
     <w lemma="with" pos="acp" xml:id="A46482-001-a-2230">with</w>
     <w lemma="they" pos="pno" xml:id="A46482-001-a-2240">them</w>
     <pc unit="sentence" xml:id="A46482-001-a-2250">.</pc>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-2260">And</w>
     <w lemma="there" pos="av" xml:id="A46482-001-a-2270">there</w>
     <w lemma="remain" pos="vvz" xml:id="A46482-001-a-2280">remains</w>
     <w lemma="only" pos="av-j" xml:id="A46482-001-a-2290">only</w>
     <w lemma="for" pos="acp" xml:id="A46482-001-a-2300">for</w>
     <w lemma="i" pos="pno" xml:id="A46482-001-a-2310">me</w>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-2320">to</w>
     <w lemma="thank" pos="vvi" xml:id="A46482-001-a-2330">thank</w>
     <w lemma="you" pos="pn" xml:id="A46482-001-a-2340">you</w>
     <pc xml:id="A46482-001-a-2350">,</pc>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-2360">and</w>
     <w lemma="all" pos="d" xml:id="A46482-001-a-2370">all</w>
     <w lemma="those" pos="d" xml:id="A46482-001-a-2380">those</w>
     <pc xml:id="A46482-001-a-2390">,</pc>
     <w lemma="both" pos="d" xml:id="A46482-001-a-2400">both</w>
     <w lemma="officer" pos="n2" xml:id="A46482-001-a-2410">Officers</w>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-2420">and</w>
     <w lemma="soldier" pos="n2" reg="Soldiers" xml:id="A46482-001-a-2430">Souldiers</w>
     <pc xml:id="A46482-001-a-2440">,</pc>
     <w lemma="who" pos="crq" xml:id="A46482-001-a-2450">who</w>
     <w lemma="have" pos="vvb" xml:id="A46482-001-a-2460">have</w>
     <w lemma="stick" pos="vvn" xml:id="A46482-001-a-2470">stuck</w>
     <w lemma="to" pos="acp" xml:id="A46482-001-a-2480">to</w>
     <w lemma="i" pos="pno" xml:id="A46482-001-a-2490">me</w>
     <pc xml:id="A46482-001-a-2500">,</pc>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-2510">and</w>
     <w lemma="be" pos="vvn" xml:id="A46482-001-a-2520">been</w>
     <w lemma="true" pos="av-j" xml:id="A46482-001-a-2530">truly</w>
     <w lemma="loyal" pos="j" xml:id="A46482-001-a-2540">loyal</w>
     <pc xml:id="A46482-001-a-2550">;</pc>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-2560">and</w>
     <w lemma="hope" pos="vvb" xml:id="A46482-001-a-2570">hope</w>
     <w lemma="you" pos="pn" xml:id="A46482-001-a-2580">you</w>
     <w lemma="will" pos="vmb" xml:id="A46482-001-a-2590">will</w>
     <w lemma="still" pos="av" xml:id="A46482-001-a-2600">still</w>
     <w lemma="retain" pos="vvi" xml:id="A46482-001-a-2610">retain</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-2620">the</w>
     <w lemma="same" pos="d" xml:id="A46482-001-a-2630">same</w>
     <w lemma="fidelity" pos="n1" xml:id="A46482-001-a-2640">Fidelity</w>
     <w lemma="to" pos="acp" xml:id="A46482-001-a-2650">to</w>
     <w lemma="i" pos="pno" xml:id="A46482-001-a-2660">me</w>
     <pc xml:id="A46482-001-a-2670">:</pc>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-2680">And</w>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-2690">to</w>
     <hi xml:id="A46482-e160">
      <w lemma="i" pos="pns" xml:id="A46482-001-a-2700">I</w>
     </hi>
     <w lemma="do" pos="vvb" xml:id="A46482-001-a-2710">do</w>
     <w lemma="not" pos="xx" xml:id="A46482-001-a-2720">not</w>
     <w lemma="expect" pos="vvi" xml:id="A46482-001-a-2730">expect</w>
     <w lemma="you" pos="pn" xml:id="A46482-001-a-2740">you</w>
     <w lemma="shall" pos="vmd" xml:id="A46482-001-a-2750">should</w>
     <w lemma="expose" pos="vvi" xml:id="A46482-001-a-2760">expose</w>
     <w lemma="yourselves" pos="pr" reg="yourselves" xml:id="A46482-001-a-2770">your selves</w>
     <pc xml:id="A46482-001-a-2790">,</pc>
     <w lemma="by" pos="acp" xml:id="A46482-001-a-2800">by</w>
     <w lemma="resist" pos="vvg" xml:id="A46482-001-a-2810">resisting</w>
     <w lemma="a" pos="d" xml:id="A46482-001-a-2820">a</w>
     <w lemma="foreign" pos="j" xml:id="A46482-001-a-2830">foreign</w>
     <w lemma="army" pos="n1" xml:id="A46482-001-a-2840">Army</w>
     <pc xml:id="A46482-001-a-2850">,</pc>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-2860">and</w>
     <w lemma="a" pos="d" xml:id="A46482-001-a-2870">a</w>
     <w lemma="poison" pos="j-vn" xml:id="A46482-001-a-2880">poisoned</w>
     <w lemma="nation" pos="n1" xml:id="A46482-001-a-2890">Nation</w>
     <pc xml:id="A46482-001-a-2900">;</pc>
     <w lemma="yet" pos="av" xml:id="A46482-001-a-2910">yet</w>
     <hi xml:id="A46482-e170">
      <w lemma="i" pos="pns" xml:id="A46482-001-a-2920">I</w>
     </hi>
     <w lemma="hope" pos="vvb" xml:id="A46482-001-a-2930">hope</w>
     <w lemma="your" pos="po" xml:id="A46482-001-a-2940">your</w>
     <w lemma="former" pos="j" xml:id="A46482-001-a-2950">former</w>
     <w lemma="principle" pos="n2" xml:id="A46482-001-a-2960">Principles</w>
     <w lemma="be" pos="vvb" xml:id="A46482-001-a-2970">are</w>
     <w lemma="so" pos="av" xml:id="A46482-001-a-2980">so</w>
     <w lemma="root" pos="vvn" xml:id="A46482-001-a-2990">rooted</w>
     <w lemma="in" pos="acp" xml:id="A46482-001-a-3000">in</w>
     <w lemma="you" pos="pn" xml:id="A46482-001-a-3010">you</w>
     <pc xml:id="A46482-001-a-3020">,</pc>
     <w lemma="that" pos="cs" xml:id="A46482-001-a-3030">That</w>
     <w lemma="you" pos="pn" xml:id="A46482-001-a-3040">you</w>
     <w lemma="will" pos="vmb" xml:id="A46482-001-a-3050">will</w>
     <w lemma="keep" pos="vvi" xml:id="A46482-001-a-3060">keep</w>
     <w lemma="yourselves" pos="pr" reg="yourselves" xml:id="A46482-001-a-3070">your selves</w>
     <w lemma="free" pos="j" xml:id="A46482-001-a-3090">free</w>
     <w lemma="from" pos="acp" xml:id="A46482-001-a-3100">from</w>
     <w lemma="association" pos="n2" xml:id="A46482-001-a-3110">Associations</w>
     <pc xml:id="A46482-001-a-3120">,</pc>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-3130">and</w>
     <w lemma="such" pos="d" xml:id="A46482-001-a-3140">such</w>
     <w lemma="pernicious" pos="j" xml:id="A46482-001-a-3150">pernicious</w>
     <w lemma="thing" pos="n2" xml:id="A46482-001-a-3160">Things</w>
     <pc unit="sentence" xml:id="A46482-001-a-3170">.</pc>
     <w lemma="time" pos="n1" xml:id="A46482-001-a-3180">Time</w>
     <w lemma="press" pos="vvz" xml:id="A46482-001-a-3190">presses</w>
     <w lemma="i" pos="pno" xml:id="A46482-001-a-3200">me</w>
     <w lemma="so" pos="av" xml:id="A46482-001-a-3210">so</w>
     <pc xml:id="A46482-001-a-3220">,</pc>
     <w lemma="that" pos="cs" xml:id="A46482-001-a-3230">that</w>
     <hi xml:id="A46482-e180">
      <w lemma="i" pos="pns" xml:id="A46482-001-a-3240">I</w>
     </hi>
     <w lemma="can" pos="vmb" xml:id="A46482-001-a-3250">can</w>
     <w lemma="say" pos="vvi" xml:id="A46482-001-a-3260">say</w>
     <w lemma="no" pos="avx-d" xml:id="A46482-001-a-3270">no</w>
     <w lemma="more" pos="avc-d" xml:id="A46482-001-a-3280">more</w>
     <pc unit="sentence" xml:id="A46482-001-a-3290">.</pc>
    </p>
    <closer xml:id="A46482-e190">
     <signed xml:id="A46482-e200">
      <w lemma="j." pos="ab" xml:id="A46482-001-a-3300">J.</w>
      <w lemma="r." pos="ab" xml:id="A46482-001-a-3310">R.</w>
      <pc unit="sentence" xml:id="A46482-001-a-3320"/>
     </signed>
    </closer>
    <postscript xml:id="A46482-e210">
     <p xml:id="A46482-e220">
      <hi xml:id="A46482-e230">
       <w lemma="i" pos="pns" xml:id="A46482-001-a-3330">I</w>
      </hi>
      <w lemma="must" pos="vmb" xml:id="A46482-001-a-3340">Must</w>
      <w lemma="add" pos="vvi" xml:id="A46482-001-a-3350">add</w>
      <w lemma="this" pos="d" xml:id="A46482-001-a-3360">this</w>
      <pc xml:id="A46482-001-a-3370">,</pc>
      <w lemma="that" pos="cs" xml:id="A46482-001-a-3380">That</w>
      <w lemma="as" pos="acp" xml:id="A46482-001-a-3390">as</w>
      <w lemma="i" pos="pns" xml:id="A46482-001-a-3400">I</w>
      <w lemma="have" pos="vvb" xml:id="A46482-001-a-3410">have</w>
      <w lemma="always" pos="av" xml:id="A46482-001-a-3420">always</w>
      <w lemma="find" pos="vvd" xml:id="A46482-001-a-3430">found</w>
      <w lemma="you" pos="pn" xml:id="A46482-001-a-3440">you</w>
      <w lemma="loyal" pos="j" xml:id="A46482-001-a-3450">Loyal</w>
      <pc xml:id="A46482-001-a-3460">,</pc>
      <w lemma="so" pos="av" xml:id="A46482-001-a-3470">so</w>
      <w lemma="you" pos="pn" xml:id="A46482-001-a-3480">you</w>
      <w lemma="have" pos="vvb" xml:id="A46482-001-a-3490">have</w>
      <w lemma="find" pos="vvn" xml:id="A46482-001-a-3500">found</w>
      <w lemma="i" pos="pno" xml:id="A46482-001-a-3510">me</w>
      <w lemma="a" pos="d" xml:id="A46482-001-a-3520">a</w>
      <w lemma="kind" pos="j" xml:id="A46482-001-a-3530">kind</w>
      <w lemma="master" pos="n1" xml:id="A46482-001-a-3540">Master</w>
      <pc xml:id="A46482-001-a-3550">,</pc>
      <w lemma="as" pos="acp" xml:id="A46482-001-a-3560">as</w>
      <w lemma="so" pos="av" xml:id="A46482-001-a-3570">so</w>
      <w lemma="you" pos="pn" xml:id="A46482-001-a-3580">you</w>
      <w lemma="shall" pos="vmb" xml:id="A46482-001-a-3590">shall</w>
      <w lemma="still" pos="av" xml:id="A46482-001-a-3600">still</w>
      <w lemma="find" pos="vvi" xml:id="A46482-001-a-3610">find</w>
      <w lemma="i" pos="pno" xml:id="A46482-001-a-3620">me</w>
      <pc unit="sentence" xml:id="A46482-001-a-3630">.</pc>
     </p>
     <closer xml:id="A46482-e240">
      <signed xml:id="A46482-e250">
       <w lemma="j." pos="ab" xml:id="A46482-001-a-3640">J.</w>
       <w lemma="r." pos="ab" xml:id="A46482-001-a-3650">R.</w>
       <pc unit="sentence" xml:id="A46482-001-a-3660"/>
      </signed>
     </closer>
    </postscript>
   </div>
   <div type="letter" xml:id="A46482-e260">
    <opener xml:id="A46482-e270">
     <salute xml:id="A46482-e280">
      <w lemma="sir" pos="n1" xml:id="A46482-001-a-3670">SIR</w>
      <pc xml:id="A46482-001-a-3680">,</pc>
     </salute>
    </opener>
    <p xml:id="A46482-e290">
     <w lemma="have" pos="vvg" xml:id="A46482-001-a-3690">HAving</w>
     <w lemma="receive" pos="vvn" xml:id="A46482-001-a-3700">received</w>
     <w lemma="this" pos="d" xml:id="A46482-001-a-3710">this</w>
     <w lemma="morning" pos="n1" xml:id="A46482-001-a-3720">Morning</w>
     <w lemma="a" pos="d" xml:id="A46482-001-a-3730">a</w>
     <w lemma="letter" pos="n1" xml:id="A46482-001-a-3740">Letter</w>
     <w lemma="from" pos="acp" xml:id="A46482-001-a-3750">from</w>
     <w lemma="his" pos="po" xml:id="A46482-001-a-3760">his</w>
     <w lemma="majesty" pos="n1" xml:id="A46482-001-a-3770">Majesty</w>
     <pc xml:id="A46482-001-a-3780">,</pc>
     <w lemma="with" pos="acp" xml:id="A46482-001-a-3790">with</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-3800">the</w>
     <w lemma="unfortunate" pos="j" xml:id="A46482-001-a-3810">unfortunate</w>
     <w lemma="news" pos="n1" xml:id="A46482-001-a-3820">News</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-3830">of</w>
     <w lemma="his" pos="po" xml:id="A46482-001-a-3840">his</w>
     <w lemma="resolution" pos="n1" xml:id="A46482-001-a-3850">Resolution</w>
     <pc xml:id="A46482-001-a-3860">,</pc>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-3870">to</w>
     <w lemma="go" pos="vvi" xml:id="A46482-001-a-3880">go</w>
     <w lemma="out" pos="av" xml:id="A46482-001-a-3890">out</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-3900">of</w>
     <hi xml:id="A46482-e300">
      <w lemma="England" pos="nn1" xml:id="A46482-001-a-3910">England</w>
      <pc xml:id="A46482-001-a-3920">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A46482-001-a-3930">and</w>
     <w lemma="that" pos="cs" xml:id="A46482-001-a-3940">that</w>
     <w lemma="he" pos="pns" xml:id="A46482-001-a-3950">he</w>
     <w lemma="be" pos="vvz" xml:id="A46482-001-a-3960">is</w>
     <w lemma="actual" pos="av-j" xml:id="A46482-001-a-3970">actually</w>
     <w lemma="go" pos="vvn" reg="gone" xml:id="A46482-001-a-3980">gon</w>
     <pc xml:id="A46482-001-a-3990">;</pc>
     <w lemma="i" pos="pns" xml:id="A46482-001-a-4000">I</w>
     <w lemma="think" pos="vvd" xml:id="A46482-001-a-4010">thought</w>
     <w lemma="myself" pos="pr" reg="myself" xml:id="A46482-001-a-4020">my self</w>
     <w lemma="oblige" pos="vvn" xml:id="A46482-001-a-4040">obliged</w>
     <pc join="right" xml:id="A46482-001-a-4050">(</pc>
     <w lemma="be" pos="vvg" xml:id="A46482-001-a-4060">being</w>
     <w lemma="at" pos="acp" xml:id="A46482-001-a-4070">at</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-4080">the</w>
     <w lemma="head" pos="n1" xml:id="A46482-001-a-4090">Head</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-4100">of</w>
     <w lemma="his" pos="po" xml:id="A46482-001-a-4110">his</w>
     <w lemma="army" pos="n1" xml:id="A46482-001-a-4120">Army</w>
     <pc xml:id="A46482-001-a-4130">)</pc>
     <w lemma="have" pos="vvg" xml:id="A46482-001-a-4140">having</w>
     <w lemma="receive" pos="vvn" xml:id="A46482-001-a-4150">received</w>
     <w lemma="order" pos="n2" xml:id="A46482-001-a-4160">Orders</w>
     <w lemma="from" pos="acp" xml:id="A46482-001-a-4170">from</w>
     <w lemma="his" pos="po" xml:id="A46482-001-a-4180">his</w>
     <w lemma="majesty" pos="n1" xml:id="A46482-001-a-4190">Majesty</w>
     <pc xml:id="A46482-001-a-4200">,</pc>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-4210">to</w>
     <w lemma="make" pos="vvi" xml:id="A46482-001-a-4220">make</w>
     <w lemma="no" pos="dx" xml:id="A46482-001-a-4230">no</w>
     <w lemma="opposition" pos="n1" xml:id="A46482-001-a-4240">Opposition</w>
     <w lemma="against" pos="acp" xml:id="A46482-001-a-4250">against</w>
     <w lemma="any" pos="d" xml:id="A46482-001-a-4260">any</w>
     <w lemma="body" pos="n1" xml:id="A46482-001-a-4270">body</w>
     <pc xml:id="A46482-001-a-4280">,</pc>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-4290">to</w>
     <w lemma="let" pos="vvi" xml:id="A46482-001-a-4300">let</w>
     <w lemma="your" pos="po" xml:id="A46482-001-a-4310">your</w>
     <w lemma="highness" pos="n1" xml:id="A46482-001-a-4320">Highness</w>
     <w lemma="know" pos="vvb" xml:id="A46482-001-a-4330">know</w>
     <pc join="right" xml:id="A46482-001-a-4340">(</pc>
     <w lemma="with" pos="acp" xml:id="A46482-001-a-4350">with</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-4360">the</w>
     <w lemma="advice" pos="n1" xml:id="A46482-001-a-4370">Advice</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-4380">of</w>
     <w lemma="all" pos="d" xml:id="A46482-001-a-4390">all</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-4400">the</w>
     <w lemma="officer" pos="n2" xml:id="A46482-001-a-4410">Officers</w>
     <pc xml:id="A46482-001-a-4420">)</pc>
     <w lemma="as" pos="acp" xml:id="A46482-001-a-4430">as</w>
     <w lemma="soon" pos="av" xml:id="A46482-001-a-4440">soon</w>
     <w lemma="as" pos="acp" xml:id="A46482-001-a-4450">as</w>
     <w lemma="it" pos="pn" xml:id="A46482-001-a-4460">it</w>
     <w lemma="be" pos="vvd" xml:id="A46482-001-a-4470">was</w>
     <w lemma="possible" pos="j" xml:id="A46482-001-a-4480">possible</w>
     <pc xml:id="A46482-001-a-4490">,</pc>
     <w lemma="to" pos="prt" xml:id="A46482-001-a-4500">to</w>
     <w lemma="hinder" pos="vvi" xml:id="A46482-001-a-4510">hinder</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-4520">the</w>
     <w lemma="misfortune" pos="n1" xml:id="A46482-001-a-4530">Misfortune</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-4540">of</w>
     <w lemma="effusion" pos="n1" xml:id="A46482-001-a-4550">Effusion</w>
     <w lemma="of" pos="acp" xml:id="A46482-001-a-4560">of</w>
     <w lemma="blood." pos="ab" xml:id="A46482-001-a-4570">Blood.</w>
     <w lemma="i" pos="pns" xml:id="A46482-001-a-4580">I</w>
     <w lemma="have" pos="vvb" xml:id="A46482-001-a-4590">have</w>
     <w lemma="send" pos="vvn" xml:id="A46482-001-a-4600">sent</w>
     <w lemma="to" pos="acp" xml:id="A46482-001-a-4610">to</w>
     <w lemma="that" pos="d" xml:id="A46482-001-a-4620">that</w>
     <w lemma="purpose" pos="n1" xml:id="A46482-001-a-4630">Purpose</w>
     <w lemma="to" pos="acp" xml:id="A46482-001-a-4640">to</w>
     <w lemma="all" pos="d" xml:id="A46482-001-a-4650">all</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-4660">the</w>
     <w lemma="troop" pos="n2" xml:id="A46482-001-a-4670">Troops</w>
     <w lemma="that" pos="cs" xml:id="A46482-001-a-4680">that</w>
     <w lemma="be" pos="vvb" xml:id="A46482-001-a-4690">are</w>
     <w lemma="under" pos="acp" xml:id="A46482-001-a-4700">under</w>
     <w lemma="my" pos="po" xml:id="A46482-001-a-4710">my</w>
     <w lemma="command" pos="n1" xml:id="A46482-001-a-4720">Command</w>
     <pc xml:id="A46482-001-a-4730">;</pc>
     <w lemma="which" pos="crq" xml:id="A46482-001-a-4740">which</w>
     <w lemma="shall" pos="vmb" xml:id="A46482-001-a-4750">shall</w>
     <w lemma="be" pos="vvi" xml:id="A46482-001-a-4760">be</w>
     <w lemma="the" pos="d" xml:id="A46482-001-a-4770">the</w>
     <w lemma="last" pos="ord" xml:id="A46482-001-a-4780">last</w>
     <w lemma="order" pos="n1" xml:id="A46482-001-a-4790">Order</w>
     <w lemma="they" pos="pns" xml:id="A46482-001-a-4800">they</w>
     <w lemma="shall" pos="vmb" xml:id="A46482-001-a-4810">shall</w>
     <w lemma="receive" pos="vvi" xml:id="A46482-001-a-4820">receive</w>
     <w lemma="from" pos="acp" xml:id="A46482-001-a-4830">from</w>
     <pc xml:id="A46482-001-a-4840">,</pc>
    </p>
    <closer xml:id="A46482-e310">
     <dateline xml:id="A46482-e320">
      <w lemma="Uxbridge" pos="nn1" xml:id="A46482-001-a-4850">Uxbridge</w>
      <pc xml:id="A46482-001-a-4860">,</pc>
      <date xml:id="A46482-e330">
       <w lemma="decemb." pos="ab" xml:id="A46482-001-a-4870">Decemb.</w>
       <w lemma="11." pos="crd" xml:id="A46482-001-a-4880">11.</w>
       <w lemma="1688." pos="crd" xml:id="A46482-001-a-4890">1688.</w>
       <pc unit="sentence" xml:id="A46482-001-a-4900"/>
      </date>
      <hi xml:id="A46482-e340">
       <w lemma="at" pos="acp" xml:id="A46482-001-a-4910">At</w>
       <w lemma="noon" pos="n1" xml:id="A46482-001-a-4920">Noon</w>
      </hi>
     </dateline>
    </closer>
   </div>
  </body>
 </text>
</TEI>
